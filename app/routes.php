<?php

# CSRF Protection
Route::when('*', 'csrf', ['POST', 'PUT', 'PATCH', 'DELETE']);

# Static Pages. Redirecting admin so admin cannot access these pages.
Route::group(['before' => 'redirectAdmin'], function()
{
	Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);
	Route::get('/about', ['as' => 'about', 'uses' => 'PagesController@getAbout']);
	Route::get('/contact', ['as' => 'contact', 'uses' => 'PagesController@getContact']);
});

//belum dicobain
# Static Pages. Redirecting operator so operator cannot access these pages.
Route::group(['before' => 'redirectOperator'], function()
{
	Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);
	Route::get('/about', ['as' => 'about', 'uses' => 'PagesController@getAbout']);
	Route::get('/contact', ['as' => 'contact', 'uses' => 'PagesController@getContact']);
});

# Registration
Route::group(['before' => 'guest'], function()
{
	Route::get('/register', 'RegistrationController@create');
	Route::post('/register', ['as' => 'registration.store', 'uses' => 'RegistrationController@store']);
});

# Authentication
Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create'])->before('guest');
Route::get('logout', ['as' => 'logout', 'uses' => 'SessionsController@destroy']);
Route::resource('sessions', 'SessionsController' , ['only' => ['create','store','destroy']]);

# Forgotten Password //belum harus pake email. config>mail
Route::group(['before' => 'guest'], function()
{
	Route::get('forgot_password', 'RemindersController@getRemind');
	Route::post('forgot_password','RemindersController@postRemind');
	Route::get('reset_password/{token}', 'RemindersController@getReset');
	Route::post('reset_password/{token}', 'RemindersController@postReset');
});


# Alumni Routes
Route::group(['before' => 'auth|alumni'], function()
{
    //Route::get('/', ['as' => 'alumni_dashboard', 'uses' => 'AlumniController@getUserProtected']);
    Route::get('lowongan', ['as' => 'lowongan', 'uses' => 'AlumniController@getLowongan']);
    Route::get('sekolah', ['as' => 'sekolah', 'uses' => 'AlumniController@getSekolah']);
    Route::resource('profiles', 'AlumniUsersController', ['only' => ['show', 'edit', 'update']]);
    Route::get('profiles/{id}/editb', ['as' => 'editb', 'uses' => 'AlumniUsersController@editb']);
    Route::patch('profiles/{id}/updateb', ['as' => 'updateb', 'uses' => 'AlumniUsersController@updateb']);


});

# Admin Routes
Route::group(['before' => 'auth|admin'], function()
{
	Route::get('/admin', ['as' => 'admin_dashboard', 'uses' => 'AdminController@getHome']);

    Route::resource('admin/profiles', 'AdminUsersController', ['only' => ['index', 'show', 'edit', 'update', 'destroy','create','store']]);
    
    Route::resource('admin/sekolah', 'AdminSekolahController', ['only' => ['index', 'create','store','show','edit','update']]);

    Route::get('admin/jurusan/createoperator/{id}', array('as'=>'admin.jurusan.createoperator', 'uses' => 'AdminJurusanController@createOperator'));
    Route::post('admin/jurusan/storeoperator/{ids}', array('as'=>'admin.jurusan.storeoperator', 'uses'=>'AdminJurusanController@storeOperator'));
    Route::get('admin/jurusan/editoperator/{id}', array('as'=>'admin.jurusan.editoperator','uses' => 'AdminJurusanController@editOperator'));
    Route::patch('admin/jurusan/updateoperator/{id}', array('as'=>'admin.jurusan.updateoperator','uses' => 'AdminJurusanController@updateOperator'));
    Route::resource('admin/jurusan', 'AdminJurusanController', ['only' => ['show','edit','update']]);    
    Route::get('admin/jurusan/create/{id}', array('as'=>'admin.jurusan.create', 'uses' => 'AdminJurusanController@create'));
    Route::post('admin/jurusan/store/{id}', array('as'=>'admin.jurusan.store', 'uses'=>'AdminJurusanController@store'));
    Route::get('admin/jurusan/detail/{id}', array('as'=>'admin.jurusan.detail', 'uses' => 'AdminJurusanController@detail'));

    Route::resource('admin/alumni', 'AdminAlumniController', ['only' => ['show','edit','update']]);
    Route::get('admin/alumni/create/{id}', array('as'=>'admin.alumni.create', 'uses' => 'AdminAlumniController@create'));
    Route::post('admin/alumni/store/{id}', array('as'=>'admin.alumni.store', 'uses'=>'AdminAlumniController@store'));    
    Route::get('admin/alumni/detail/{id}', array('as'=>'admin.alumni.detail', 'uses' => 'AdminAlumniController@detail'));

    //ada bug kalo ditaro dibawah AdminLokerController@show
    Route::get('admin/loker/createglobal', array('as'=>'admin.loker.createglobal', 'uses' => 'AdminLokerController@createGlobal'));
    Route::post('admin/loker/storeglobal', array('as'=>'admin.loker.storeglobal', 'uses'=>'AdminLokerController@storeGlobal'));
    Route::resource('admin/loker', 'AdminLokerController', ['only' => ['index','show','edit','update']]);
    Route::get('admin/loker/create/{id}', array('as'=>'admin.loker.create', 'uses' => 'AdminLokerController@create'));
    Route::post('admin/loker/store/{id}', array('as'=>'admin.loker.store', 'uses'=>'AdminLokerController@store'));
    Route::get('admin/loker/detail/{id}', array('as'=>'admin.loker.detail', 'uses'=>'AdminLokerController@detail'));

});

# Operator Routes
Route::group(['before' => 'auth|operator'], function()
{
    Route::get('/operator', ['as' => 'operator_dashboard', 'uses' => 'OperatorController@getHome']);
    Route::resource('operator/profiles', 'OperatorUsersController', ['only' => ['index', 'show', 'edit', 'update', 'destroy']]);
    Route::resource('operator/lowongan', 'OperatorLowonganController', ['only' => ['index', 'create','store','show','edit','update']]);
    Route::get('operator/lowongan/deactive/{id}', ['as'=>'deactivelowongan', 'uses'=>'OperatorLowonganController@deactive']);
    Route::get('operator/pending', ['as'=>'pendinguser','uses'=>'OperatorUsersController@pending']);
    Route::get('operator/approval/{id}/{user}', ['as'=>'approveuser','uses'=>'OperatorUsersController@approval']);
    Route::get('operator/decline/{id}/{user}', ['as'=>'declineuser','uses'=>'OperatorUsersController@decline']);
});

