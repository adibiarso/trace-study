@extends('protected.admin.master')

@section('title', 'View Jurusan')

@section('content')
	<h1>{{ $dataloker->sekolah->nama }}</h1>
	<h2>{{ $dataloker->instansi }} - {{$dataloker->posisi}}</h2>
	<ul>
		<li>ID: {{$dataloker->id}}</li>
		<li>Sekolah: {{$dataloker->sekolah->nama}}</li>
		<li>Link: {{ $dataloker->link }}</li>
		<li>Instansi: {{$dataloker->instansi}}</li>
		<li>Posisi: {{$dataloker->posisi}}</li>
		<li>Deskripsi: {{$dataloker->deskripsi}}</li>
		<li>Created By: {{ $dataloker->lowongankerjacreatedby->username }}</li>
		<li>Last Modified By: {{$dataloker->lowongankerjamodifiedby->username}}</li>
		<li>Aktivasi: 
			@if($dataloker->aktif == 1)
				{{"Lowongan Kerja Aktif"}}
			@else
				{{"Lowongan Kerja Tidak Aktif"}}
			@endif
		</li> 
	</ul>

	@if(Sentry::check())

		{{ link_to_route('admin.loker.edit', 'Edit', $dataloker->id, ['class' => 'btn btn-primary']) }}
					
	@endif
	{{ HTML::link('admin/loker/'.$dataloker->sekolah_id, 'Back') }}

@stop