@extends('protected.admin.master')

@section('title', 'Tambah Alumni')

@section('content')


		<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Tambah Alumni - {{$datajurusan->nama}} - {{$datajurusan->sekolah->nama}}</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['method' => 'post', 'route' => ['admin.alumni.store', $datajurusan->id]]) }}
	                    <fieldset>
	                    	@if (Session::has('flash_message'))
								<div class="form-group">
									<p>{{ Session::get('flash_message') }}</p>
								</div>
							@endif

							<!-- MASUK KE TABEL USERS -->
							<div class="form-group">
								{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('email', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('username', null, ['placeholder' => 'Username', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('username', $errors) }}
							</div>

							<!-- Password field -->
							<div class="form-group">
								{{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('password', $errors) }}
							</div>

							<!-- Password Confirmation field -->
							<div class="form-group">
								{{ Form::password('password_confirmation', ['placeholder' => 'Password Confirm', 'class' => 'form-control', 'required' => 'required'])}}

							</div>

							<!-- First name field -->
							<div class="form-group">
								{{ Form::text('fname', null, ['placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('fname', $errors) }}
							</div>

							<!-- Last name field -->
							<div class="form-group">
								{{ Form::text('lname', null, ['placeholder' => 'Last Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('lname', $errors) }}
							</div>
							<!-- END TABEL USERS -->


							<!-- MASUK TABEL ALUMNI -->
							<div class="form-group">
								{{ Form::text('nis', null, ['placeholder' => 'NIS', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('nis', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('tanggallahir', null, ['placeholder' => 'Tanggal Lahir', 'class' => 'form-control'])}}
								{{ errors_for('tanggallahir', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('tempatlahir', null, ['placeholder' => 'Tempat Lahir', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('tempatlahir', $errors) }}
							</div>
							<div class="form-group">								
								{{ Form::select('gender', array('L' => 'Laki-laki', 'P' => 'Perempuan'), null, ['class' => 'form-control']) }}
								{{ errors_for('gender', $errors)}}
							</div>

							<div class="form-group">
								{{ Form::text('fb', null, ['placeholder' => 'Username Facebook', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('fb', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('nohp', null, ['placeholder' => 'Nomor HP', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('nohp', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('alamat', null, ['placeholder' => 'Alamat', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('alamat', $errors) }}
							</div>
							<div class="form-group">
								{{ Form::text('telepon', null, ['placeholder' => 'Telepon', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('telepon', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('recruitmentsekolah', null, ['placeholder' => 'Recruitment Sekolah', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('recruitmentsekolah', $errors) }}
							</div>
							<div class="form-group">								
								{{ Form::select('statusbekerja', array('1' => 'Sedang Bekerja', '0' => 'Tidak Bekerja'), null, ['class' => 'form-control']) }}
								{{ errors_for('statusbekerja', $errors)}}
							</div>

							<div class="form-group">
								{{ Form::text('kegiatankelompok', null, ['placeholder' => 'Kegiatan Kelompok', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('kegiatankelompok', $errors) }}
							</div>
							<div class="form-group">								
								{{ Form::select('minatwirausaha', array('1' => 'Saya Berminat Wirausaha', '0' => 'Saya Tidak Berminat Wirausaha'), null, ['class' => 'form-control']) }}
								{{ errors_for('minatwirausaha', $errors)}}
							</div>

							<div class="form-group">
								{{ Form::label('aktifform', 'Status Aktivasi:')}}
								{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), array('1'), ['class' => 'form-control']) }}
								{{ errors_for('aktifform', $errors)}}
							</div>

							<!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Tambah', ['class' => 'btn btn-lg btn-primary btn-block']) }}
								{{ HTML::link('admin/alumni'.$datajurusan->id, 'Back') }}
							</div>
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
			</div>
		</div>
	</div>



@stop