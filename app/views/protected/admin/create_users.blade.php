@extends('protected.admin.master')

@section('title', 'Tambah User')

@section('content')


		<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Tambah User</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['method' => 'post', 'route' => 'admin.profiles.store']) }}
	                    <fieldset>
	                    	@if (Session::has('flash_message'))
								<div class="form-group">
									<p>{{ Session::get('flash_message') }}</p>
								</div>
							@endif
							
							<div class="form-group">
								{{ Form::label('email', 'Email:')}}
								{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('email', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::label('username', 'Username:')}}
								{{ Form::text('username', null, ['placeholder' => 'Username', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('username', $errors) }}
							</div>

							<!-- Password field -->
							<div class="form-group">
								{{ Form::label('password', 'Password:')}}
								{{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('password', $errors) }}
							</div>

							<!-- Password Confirmation field -->
							<div class="form-group">
								{{ Form::label('password_confirmation', 'Confirm Password:')}}
								{{ Form::password('password_confirmation', ['placeholder' => 'Password Confirm', 'class' => 'form-control', 'required' => 'required'])}}

							</div>

							<!-- First name field -->
							<div class="form-group">
								{{ Form::label('fname', 'First Name:')}}
								{{ Form::text('fname', null, ['placeholder' => 'First Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('fname', $errors) }}
							</div>

							<!-- Last name field -->
							<div class="form-group">
								{{ Form::label('lname', 'Last Name:')}}
								{{ Form::text('lname', null, ['placeholder' => 'Last Name', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('lname', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::label('groupregister', 'User Sebagai:')}}
								{{ Form::select('groupregister', array('1' => 'Admin', '2' => 'Operator', '3' => 'Alumni'), array('1'), ['class' => 'form-control']) }}
								{{ errors_for('groupregister', $errors)}}
							</div>

							<div class="form-group">
								{{ Form::label('sekolah_id', '(Bila Mengisi Operator) Operator di Sekolah:')}}
								{{ Form::select('sekolah_id', $sekolahs, null, ['class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('sekolah_id', $errors)}}
							</div>

							<div class="form-group">
								{{ Form::label('aktifform', 'Status Aktivasi:')}}
								{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), array('1'), ['class' => 'form-control']) }}
								{{ errors_for('aktifform', $errors)}}
							</div>

							<!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Tambah', ['class' => 'btn btn-lg btn-primary btn-block']) }}
								{{ HTML::link('admin/loker', 'Back') }}
							</div>
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
			</div>
		</div>
	</div>



@stop