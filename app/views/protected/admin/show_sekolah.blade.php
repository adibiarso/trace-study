@extends('protected.admin.master')

@section('title', 'View Sekolah')

@section('content')

	<h1>{{ $listsekolah->nama }}'s Profile 
	@if($listsekolah->logo)
	{{HTML::image("$listsekolah->logo", $alt="$listsekolah->nama", array( 'width' => 70, 'height' => 70 ))}}
	@endif
	</h1>
	<ul>
		<li>ID: {{$listsekolah->id}}</li>
		<li>Nama: {{ $listsekolah->nama }}</li>
		<li>Logo: {{ $listsekolah->logo }}</li>
		<li>Alamat: {{ $listsekolah->alamat }}</li>
		<li>Telepon: {{ $listsekolah->telepon }}</li>
		<li>Akreditasi: {{ $listsekolah->akreditasi }}</li>
		<li>Jumlah Pengajar: {{ $listsekolah->jml_pengajar }}</li>
		<li>Deskripsi: {{ $listsekolah->deskripsi }}</li>
		<li>Created By: {{ $listsekolah->sekolahcreatedby->username }}</li>
		<li>Last Modified By: {{$listsekolah->sekolahmodifiedby->username}}</li>
		<li>Aktivasi: 
			@if($listsekolah->aktif == 1)
				{{"Sekolah Aktif"}}
			@else
				{{"Sekolah Tidak Aktif"}}
			@endif
		</li> 
	</ul>

	@if(Sentry::check())

		{{ link_to_route('admin.sekolah.edit', 'Edit', $listsekolah->id, ['class' => 'btn btn-primary']) }}
					
	@endif
	{{ HTML::link('admin/sekolah/', 'Back') }}

@stop