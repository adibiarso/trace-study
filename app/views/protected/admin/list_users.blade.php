@extends('protected.admin.master')

@section('title', 'List Users')

@section('content')

<h2>Registered Users</h2>
	<p>Here you would normally search for users but since this is just a demo, I'm listing all of them.</p>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>id</th>
	          <th>Email</th>
	          <th>Username</th>
	          <th>First Name</th>
	          <th>Last Name</th>
	          <th>Status</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($users as $user)
      		<tr>
	      		<td>{{ $user->id }}</td>
		        <td>{{ link_to_route('admin.profiles.show', $user->email, $user->id) }}<br>
 				@if ($user->inGroup($groupadmin))
		        	<span class="label label-success">{{ 'Admin' }}</span>
		        @elseif ($user->inGroup($groupoperator))
		        	<span class="label label-operator">{{ 'Operator' }}</span>
		        @elseif ($user->inGroup($groupalumni))
		        	<span class="label label-alumni">{{ 'Alumni' }}</span>
		        @endif
		        </td>
		        <td>{{$user->username}}</td>
		        <td>{{ $user->first_name}}</td>
		        <td>{{ $user->last_name}}</td>
		        <td>
		        @if($user->activated == 1)
						{{"Aktif"}}
					@else
						{{"Tidak Aktif"}}
					@endif
				</td>
		     </tr>
			@endforeach

      	</tbody>
	</table>
	
		{{ HTML::link('admin/profiles/create', 'Tambah', ['class' => 'btn btn-primary']) }}
	<hr>
	<h1>Admin</h1>
<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Status</th>
					<th>Sebagai</th>
				</tr>
			</thead>
			<tbody>
			@foreach($useradmin as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ link_to_route('admin.profiles.show', $user->email, $user->id) }}<br>
		        	<span class="label label-success">{{ 'Admin' }}
		        	</td>
					<td>{{ $user->username }}</td>
					<td>{{ $user->first_name }}</td>
					<td>{{ $user->last_name }}</td>
					<td>
		       		@if($user->activated == 1)
						{{"Aktif"}}
					@else
						{{"Tidak Aktif"}}
					@endif
					</td>
					<td>Admin</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<hr>
	<h1>Operator</h1>
<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Status</th>
					<th>Sebagai</th>
				</tr>
			</thead>
			<tbody>
			@foreach($useroperator as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ link_to_route('admin.profiles.show', $user->email, $user->id) }}<br>
					<span class="label label-operator">{{ 'Operator' }}
		        	</td>
					<td>{{ $user->username }}</td>
					<td>{{ $user->first_name }}</td>
					<td>{{ $user->last_name }}</td>
					<td>.label-success{background-color:#5cb85c}
		       		@if($user->activated == 1)
						{{"Aktif"}}
					@else
						{{"Tidak Aktif"}}
					@endif
					</td>
					<td>Operator</td>
				</tr>
			@endforeach
			</tbody>
		</table><br>
<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Status</th>
					<th>Sebagai</th>
				</tr>
			</thead>
			<tbody>
			@foreach($useralumni as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ link_to_route('admin.profiles.show', $user->email, $user->id) }}<br>
					<span class="label label-alumni">{{ 'Alumni' }}
					</td>
					<td>{{ $user->username }}</td>
					<td>{{ $user->first_name }}</td>
					<td>{{ $user->last_name }}</td>
					<td>
		       		@if($user->activated == 1)
						{{"Aktif"}}
					@else
						{{"Tidak Aktif"}}
					@endif
					</td>
					<td>Alumni</td>
				</tr>
			@endforeach
			</tbody>
		</table>
@stop