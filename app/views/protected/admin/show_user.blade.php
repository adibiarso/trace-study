@extends('protected.admin.master')

@section('title', 'View Profile')

@section('content')

	<h1>{{ $user->first_name }}'s Profile</h1>
	<ul>
		<li>Account Type: {{ $user_group }}</li>
		<li>Email Address: {{ $user->email }}</li>
		<li>First Name: {{ $user->first_name }}</li>
		<li>Last Name: {{ $user->last_name }}</li>
		@if($user_group == "Operator")
		<li>Operator di Sekolah: {{$user->sekolah->first()->nama}}</li>
		@elseif($user_group == "Alumni")
		<li>Alumni di Sekolah: {{$alumnis->first()->jurusan->sekolah->nama}}</li>
		<li>Alumni di Jurusan: {{$alumnis->first()->jurusan->nama}}</li>
		<hr>
		<li>ID Alumni: {{$alumnis->first()->id}}</li>
		<li>NIS: {{$alumnis->first()->nis}}</li>
		<li>Tanggal Lahir: {{$alumnis->first()->tanggallahir}}</li>
		<li>Tempat Lahir: {{$alumnis->first()->tempatlahir}}</li>
		<li>Gender:
			@if($alumnis->first()->gender == "L")
				{{"Laki-Laki"}}
			@elseif($alumnis->first()->gender=="P")
				{{"Perempuan"}}
			@endif
		</li>
		<li>Username Facebook: {{$alumnis->first()->fb}}</li>
		<li>Nomor HP: {{$alumnis->first()->nohp}}</li>
		<li>Alamat: {{$alumnis->first()->alamat}}</li>
		<li>Telepon: {{$alumnis->first()->telepon}}</li>
		<li>Jurusan: {{$alumnis->first()->jurusan->nama}}</li>
		<li>Sekolah: {{$alumnis->first()->jurusan->sekolah->nama}}</li>
		<li>Recruitment Sekolah(Tahun Masuk/Keluar?): {{$alumnis->first()->recruitmentsekolah}} </li>
		<li>Status Bekerja:
			@if($alumnis->first()->statusbekerja == 1)
				{{"Sedang Bekerja"}}
			@else
				{{"Tidak Bekerja"}}
			@endif
		</li>
		<li>Kegiatan Kelompok: {{$alumnis->first()->kegiatankelompok}}</li>
		<li>Created By: {{$alumnis->first()->userscreatedby->username}}</li>
		<li>Last Modified By: {{$alumnis->first()->usersmodifiedby->username}}</li>
		<li>Minat Wirausaha:
		@if($alumnis->first()->minatwirausaha == 1)
				{{"Berminat Wirausaha"}}
			@else
				{{"Tidak Berminat Wirausaha"}}
			@endif
		</li>
		@endif
	</ul>

	@if(Sentry::check())

		{{ link_to_route('admin.profiles.edit', 'Edit Profile', $user->id, ['class' => 'btn btn-primary']) }}

	@endif

@stop