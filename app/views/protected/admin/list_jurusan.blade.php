@extends('protected.admin.master')

@section('title', 'List Jurusan')

@section('content')

<div>
	@foreach($listsekolah as $sekolah)
	<h2>{{ $sekolah->nama }}</h2>
	@endforeach

@if ($listjurusan->isEmpty())
		<p>There are no Jurusan! :(</p>
@else
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>					
					<th>Nama Jurusan</th>
					<th>Jumlah Alumni</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			@foreach($listjurusan as $jurusan)
				<tr>
					<td>{{ link_to_route('admin.alumni.show', $jurusan->nama, $jurusan->id) }}</td>
					
					<td>{{count($jurusan->alumni)}}</td>
					<td>
						@if($jurusan->aktif == 1)
							{{"Jurusan Aktif"}}
						@else
							{{"Jurusan Tidak Aktif"}}
						@endif
					</td>
					<td>{{ link_to_route('admin.jurusan.edit', 'Edit', $jurusan->id, ['class' => 'btn btn-primary']) }}
						{{ link_to_route('admin.jurusan.detail', 'Details', $jurusan->id, ['class' => 'btn btn-primary']) }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
@endif
		{{ HTML::link('admin/jurusan/create/'.$sekolah->id, 'Tambah Jurusan', ['class' => 'btn btn-primary']) }}
</div>

<div>		
	<br><br><h3>Daftar Operator {{ $sekolah->nama }}</h3>

		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					
					
					<th>Activated</th>
					
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
			@foreach($listsekolah as $sekolah)
				@foreach($sekolah->users as $sekolahuser)
				<tr>
					<td>{{ $sekolahuser->id}}</td>
					<td>{{ $sekolahuser->email }}</td>
					<td>{{ $sekolahuser->username }}</td>
					<td>{{ $sekolahuser->first_name }}</td>
					<td>{{ $sekolahuser->last_name }}</td>
					<td>
						@if ( $sekolahuser->activated  == 1)
							{{'1'}}
						@else
							{{'0'}}
						@endif
					</td>
					<td>{{ link_to_route('admin.jurusan.editoperator', 'Edit', $sekolahuser->id, ['class' => 'btn btn-primary']) }}</td>
				</tr>
				@endforeach
			@endforeach
			</tbody>
		</table>
<br>
{{ HTML::link('admin/jurusan/createoperator/'.$sekolah->id, 'Tambah Operator', ['class' => 'btn btn-primary']) }}
</div><br>
{{ HTML::link('admin/sekolah/', 'Back', ['class' => 'btn btn-primary']) }}


@stop

