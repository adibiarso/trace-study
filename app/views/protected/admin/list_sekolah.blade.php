@extends('protected.admin.master')

@section('title', 'List Sekolah')

@section('content')

<h2>Daftar Sekolah</h2>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>Nama</th>
			  <th>Logo</th>
			  <th>Alamat</th>
			  <th>Status</th>
			  <th>Aksi</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($listsekolah as $sekolah)
      		<tr>
	      		<td>{{ link_to_route('admin.jurusan.show', $sekolah->nama, $sekolah->id) }}<br>
		        <td>{{HTML::image("$sekolah->logo", $alt="$sekolah->nama", $attributes = array())}}</td>
				<td>{{ $sekolah->alamat }}</td>
				<td>
					@if($sekolah->aktif == 1)
						{{"Sekolah Aktif"}}
					@else
						{{"Sekolah Tidak Aktif"}}
					@endif
				</td>
				<td>{{ link_to_route('admin.sekolah.edit', 'Edit', $sekolah->id, ['class' => 'btn btn-primary']) }}
					{{ link_to_route('admin.sekolah.show', 'Details', $sekolah->id, ['class' => 'btn btn-primary']) }}
				</td>
		     </tr>
			@endforeach
      	</tbody>
	</table>
	
			{{ link_to_route('admin.sekolah.create', 'Tambah', null, ['class' => 'btn btn-primary']) }}

@stop