@extends('protected.admin.master')

@section('title', 'Tambah Sekolah')

@section('content')


		<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Tambah Sekolah</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['route' => 'admin.sekolah.store', 'files' => true, 'method' => 'post']) }}
	                    <fieldset>
	                    	@if (Session::has('flash_message'))
								<div class="form-group">
									<p>{{ Session::get('flash_message') }}</p>
								</div>
							@endif

							<div class="form-group">
								{{ Form::text('nama', null, ['placeholder' => 'Nama Sekolah', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('nama', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::label('logo', 'Logo:') }}
								{{ Form::file('logo', null, ['class' => 'form-control'])}}
								{{ errors_for('logo', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('alamat', null, ['placeholder' => 'Alamat', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('alamat', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('telepon', null, ['placeholder' => 'Nomor Telepon', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('telepon', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('akreditasi', null, ['placeholder' => 'Akreditasi Sekolah', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('akreditasi', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('jml_pengajar', null, ['placeholder' => 'Jumlah Pengajar', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('jml_pengajar', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('deskripsi', null, ['placeholder' => 'Deskripsi Sekolah', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('deskripsi', $errors) }}
							</div>

							<!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Tambah', ['class' => 'btn btn-lg btn-primary btn-block']) }}
								{{ HTML::link('admin/sekolah', 'Back') }}
							</div>
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
			</div>
		</div>
	</div>



@stop