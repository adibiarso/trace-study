@extends('protected.admin.master')

@section('title', 'List Alumni')

@section('content')

<h2>{{$jurusan->sekolah->nama}}</h2>
<h3>{{$jurusan->nama}}</h3><br>
	@if ($listalumni->isEmpty())
		<p>There are no User! :(</p>
	@else
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Username</th>
					<th>NIS</th>
					<!--<th>Tanggal Lahir</th>-->
					<!--<th>Tempat Lahir</th>-->
					<th>Gender</th>
					<th>Facebook</th>
					<th>No hp</th>
					<th>Alamat</th>
					<th>Telepon</th>
					<th>Recruitment Sekolah</th>
					<!--<th>Status Bekerja</th>-->
					<!--<th>Kegiatan Kelompok</th>-->
			 		<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			@foreach($listalumni as $alumni)
				<tr>
					<td>{{ $alumni->users->id}}</td>
					<td>{{ $alumni->users->email }}</td>
					<td>{{ $alumni->users->username }}</td>
					<td>{{ $alumni->nis }}</td>
					<!--<td>{{ $alumni->tanggallahir }}</td>-->
					<!--<td>{{ $alumni->tempatlahir }}</td>-->
					<td>{{ $alumni->gender }}</td>
					<td>{{ $alumni->fb }}</td>
					<td>{{ $alumni->nohp }}</td>
					<td>{{ $alumni-> alamat}}</td>
					<td>{{ $alumni->telepon}}</td>
					<td>{{ $alumni->recruitmentsekolah}}</td>
					<!--<td>{{ $alumni->statusbekerja}}</td>-->
					<!--<td>{{ Str::limit($alumni->kegiatankelompok, '10')}}</td>-->
					<td>
					@if($alumni->users->activated == 1)
						{{"Aktif"}}
					@else
						{{"Tidak Aktif"}}
					@endif
					</td>
					<td>{{ link_to_route('admin.alumni.edit', 'Edit', $alumni->id, ['class' => 'btn btn-primary']) }}
						{{ link_to_route('admin.alumni.detail', 'Details', $alumni->id, ['class' => 'btn btn-primary']) }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		{{ HTML::link('admin/alumni/create/'.$jurusan->id, 'Tambah Alumni', ['class' => 'btn btn-primary']) }}
	@endif

@stop