@extends('protected.admin.master')

@section('title', 'Edit Jurusan')

@section('content')
	<h1>Edit Jurusan {{ $jurusanedit->nama}}</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($jurusanedit, ['method' => 'PATCH', 'route' => ['admin.jurusan.update', $jurusanedit->id]]) }}

	<div class="form-group">
		{{ Form::label('nama', 'Nama:') }}
		{{ Form::text('nama', null, ['class' => 'form-control']) }}
		{{ errors_for('email', $errors) }}
	</div>
	<div class="form-group">
		{{ Form::label('aktifform', 'Aktivasi Jurusan:')}}
		{{ Form::select('aktifform', array('0' => 'Jurusan Tidak Aktif', '1' => 'Jurusan Aktif'), $jurusanedit->aktif, ['class' => 'form-control']) }}
		{{ errors_for('aktifform', $errors)}}
	</div>
	<!-- Update Profile Field -->
	<div class="form-group">
		{{ Form::submit('Update Jurusan', ['class' => 'btn btn-primary']) }}{{ HTML::link('admin/jurusan/'.$jurusanedit->id, 'Back') }}
	</div>

	{{ Form::close() }}

@stop