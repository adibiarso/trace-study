@extends('protected.admin.master')

@section('title', 'Edit Alumni')

@section('content')
	<h1>Edit Alumni</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($alumniedit, ['method' => 'PATCH', 'route' => ['admin.alumni.update', $alumniedit->id]]) }}
		
		<div class="form-group">
			{{ Form::label('jurusan_id', 'Jurusan:')}}
			{{ Form::select('jurusan_id', $jurusans, Input::old('jurusan_id'), ['class' => 'form-control'])}}
			{{ errors_for('jurusan_id', $errors) }}
		</div>


		<div class="form-group">
			{{ Form::label('email', 'Email:')}}
			{{ Form::text('email', $alumniedit->users->email, ['class' => 'form-control'])}}
			{{ errors_for('email', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('username', 'Username:')}}
			{{ Form::text('username', $alumniedit->users->username, ['class' => 'form-control'])}}
			{{ errors_for('username', $errors) }}
		</div>

		<!-- password ga dibuat bisa diedit -->


		<!-- First name field -->
		<div class="form-group">
			{{ Form::label('first_name', 'First Name:')}}
			{{ Form::text('first_name', $alumniedit->users->first_name, ['class' => 'form-control'])}}
			{{ errors_for('first_name', $errors) }}
		</div>

		<!-- Last name field -->
		<div class="form-group">
			{{ Form::label('last_name', 'Last Name:')}}
			{{ Form::text('last_name', $alumniedit->users->last_name, ['class' => 'form-control'])}}
			{{ errors_for('last_name', $errors) }}
		</div>
		<!-- END TABEL USERS -->


		<!-- MASUK TABEL ALUMNI -->
		<div class="form-group">
			{{ Form::label('nis', 'NIS:')}}
			{{ Form::text('nis', null, ['class' => 'form-control'])}}
			{{ errors_for('nis', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('tanggallahir', 'Tanggal Lahir:')}}
			{{ Form::text('tanggallahir', null, ['class' => 'form-control'])}}
			{{ errors_for('tanggallahir', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('tempatlahir', 'Tempat Lahir:')}}
			{{ Form::text('tempatlahir', null, ['class' => 'form-control'])}}
			{{ errors_for('tempatlahir', $errors) }}
		</div>
		<div class="form-group">
			{{ Form::label('gender', 'Jenis Kelamin:')}}								
			{{ Form::select('gender', array('L' => 'Laki-laki', 'P' => 'Perempuan'), $alumniedit->gender, ['class' => 'form-control']) }}
			{{ errors_for('gender', $errors)}}
		</div>

		<div class="form-group">
			{{ Form::label('fb', 'Username Facebook:')}}
			{{ Form::text('fb', null, ['class' => 'form-control'])}}
			{{ errors_for('fb', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('nohp', 'Nomor HP:')}}
			{{ Form::text('nohp', null, ['class' => 'form-control'])}}
			{{ errors_for('nohp', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('alamat', 'Alamat:')}}
			{{ Form::text('alamat', null, ['class' => 'form-control'])}}
			{{ errors_for('alamat', $errors) }}
		</div>
		<div class="form-group">
			{{ Form::label('telepon', 'Telepon:')}}
			{{ Form::text('telepon', null, ['class' => 'form-control'])}}
			{{ errors_for('telepon', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('recruitmentsekolah', 'Recruitment Sekolah (Tahun Masuk/Keluar?):')}}
			{{ Form::text('recruitmentsekolah', null, ['class' => 'form-control'])}}
			{{ errors_for('recruitmentsekolah', $errors) }}
		</div>
		<div class="form-group">
			{{ Form::label('statusbekerja', 'Status Bekerja:')}}								
			{{ Form::select('statusbekerja', array('1' => 'Sedang Bekerja', '0' => 'Tidak Bekerja'), $alumniedit->statusbekerja, ['class' => 'form-control']) }}
			{{ errors_for('statusbekerja', $errors)}}
		</div>

		<div class="form-group">
			{{ Form::label('kegiatankelompok', 'Kegiatan Kelompok:')}}
			{{ Form::text('kegiatankelompok', null, ['class' => 'form-control'])}}
			{{ errors_for('kegiatankelompok', $errors) }}
		</div>
		<div class="form-group">	
			{{ Form::label('minatwirausaha', 'Minat Beriwrausaha:')}}							
			{{ Form::select('minatwirausaha', array('1' => 'Saya Berminat Wirausaha', '0' => 'Saya Tidak Berminat Wirausaha'), $alumniedit->minatwirausaha, ['class' => 'form-control']) }}
			{{ errors_for('minatwirausaha', $errors)}}
		</div>

		<div class="form-group">
			{{ Form::label('aktifform', 'Status Aktivasi:')}}
			{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), $alumniedit->users->activated, ['class' => 'form-control']) }}
			{{ errors_for('aktifform', $errors)}}
		</div>

		<!-- Update Profile Field -->
		<div class="form-group">
			{{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
		</div>
	{{ Form::close() }}

@stop