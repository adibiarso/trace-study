@extends('protected.admin.master')

@section('title', 'Lowongan Kerja')

@section('content')
<div>
<h2>Daftar Sekolah</h2>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>Nama</th>
			  <!--<th>Logo</th>-->
			  <th>Alamat</th>
			  <th>Jumlah Loker(Aktif+Tidak Aktif)</th>
			  <th>Aksi</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($datasekolah as $sekolah)
      		<tr>
	      		<td>{{ $sekolah->nama }}<br>
		        <!--<td>{{HTML::image("$sekolah->logo", $alt="$sekolah->nama", $attributes = array())}}</td>-->
				<td>{{ $sekolah->alamat }}</td>
				<td>{{count($sekolah->lowongankerja)}}</td>
				<td>{{ link_to_route('admin.loker.show', 'Lihat Loker', $sekolah->id, ['class' => 'btn btn-primary']) }}
				</td>
		     </tr>
			@endforeach
      	</tbody>
	</table>
</div>

<div>
<h2>Daftar Lowongan Kerja</h2>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>ID</th>
	          <th>Sekolah</th>
	          <th>Link</th>
			  <th>Instansi Loker</th>
			  <th>Posisi</th>
			  <th>Deskripsi</th>
			  <th>Status</th>
			  <th>Aksi</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($listloker as $loker)
      		<tr>
      			<td>{{$loker->id}}</td>
	      		<td>{{ $loker->sekolah->nama }}</td>
	      		<td><a href="{{$loker->link}}">Link</a></td>
		        <td>{{ $loker->instansi}} </td>
				<td>{{ $loker->posisi }}</td>
				<td>{{ $loker->deskripsi }}</td>
				<td>
					@if($loker->aktif == 1)
						{{"Loker Aktif"}}
					@else
						{{"Loker Tidak Aktif"}}
					@endif
				</td> 
				<td>{{ link_to_route('admin.loker.edit', 'Edit', $loker->id, ['class' => 'btn btn-primary']) }}
					{{ link_to_route('admin.loker.detail', 'Details', $loker->id, ['class' => 'btn btn-primary']) }}
				</td>
		     </tr>
			@endforeach
      	</tbody>
	</table>	
			{{ link_to_route('admin.loker.createglobal', 'Tambah', null, ['class' => 'btn btn-primary']) }}
</div>

@stop