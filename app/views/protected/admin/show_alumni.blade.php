@extends('protected.admin.master')

@section('title', 'View Profile Alumni')

@section('content')

	<h1>{{ $listalumni->users->first_name }}'s Profile</h1>
	<ul>
		<li>ID User: {{$listalumni->users->id}}</li>
		<li>Email Address: {{ $listalumni->users->email }}</li>
		<li>Username: {{ $listalumni->users->username }}</li>
		<li>First Name: {{ $listalumni->users->first_name }}</li>
		<li>Last Name: {{ $listalumni->users->last_name }}</li>
		<li>Aktif:
			@if($listalumni->users->activated == 1)
				{{"Alumni Aktif"}}
			@else
				{{"Alumni Tidak Aktif"}}
			@endif
		</li>
		<li>Last Login: {{$listalumni->users->last_login}}</li>
	</ul>
	<ul>
		<li>ID Alumni: {{$listalumni->id}}</li>
		<li>NIS: {{$listalumni->nis}}</li>
		<li>Tanggal Lahir: {{$listalumni->tanggallahir}}</li>
		<li>Tempat Lahir: {{$listalumni->tempatlahir}}</li>
		<li>Gender:
			@if($listalumni->gender == "L")
				{{"Laki-Laki"}}
			@elseif($listalumni->gender=="P")
				{{"Perempuan"}}
			@endif
		</li>
		<li>Username Facebook: {{$listalumni->fb}}</li>
		<li>Nomor HP: {{$listalumni->nohp}}</li>
		<li>Alamat: {{$listalumni->alamat}}</li>
		<li>Telepon: {{$listalumni->telepon}}</li>
		<li>Jurusan: {{$listalumni->jurusan->nama}}</li>
		<li>Sekolah: {{$listalumni->jurusan->sekolah->nama}}</li>
		<li>Recruitment Sekolah(Tahun Masuk/Keluar?): {{$listalumni->recruitmentsekolah}} </li>
		<li>Status Bekerja:
			@if($listalumni->statusbekerja == 1)
				{{"Sedang Bekerja"}}
			@else
				{{"Tidak Bekerja"}}
			@endif
		</li>
		<li>Kegiatan Kelompok: {{$listalumni->kegiatankelompok}}</li>
		<li>Created By: {{$listalumni->userscreatedby->username}}</li>
		<li>Last Modified By: {{$listalumni->usersmodifiedby->username}}</li>
		<li>Minat Wirausaha:
		@if($listalumni->minatwirausaha == 1)
				{{"Berminat Wirausaha"}}
			@else
				{{"Tidak Berminat Wirausaha"}}
			@endif
		</li>
	</ul>

	@if(Sentry::check())

		{{ link_to_route('admin.alumni.edit', 'Edit Profile', $listalumni->id, ['class' => 'btn btn-primary']) }}

	@endif

@stop