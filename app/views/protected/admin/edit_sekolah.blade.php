@extends('protected.admin.master')

@section('title', 'Edit Sekolah')

@section('content')
	<h1>Edit Sekolah {{ $sekolahedit->nama}}</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($sekolahedit, ['method' => 'PATCH', 'route' => ['admin.sekolah.update', $sekolahedit->id], 'files' => true]) }}

	<div class="form-group">
		{{ Form::label('nama', 'Nama:') }}
		{{ Form::text('nama', null, ['class' => 'form-control']) }}
		{{ errors_for('email', $errors) }}
	</div>

	<div class="form-group">
		@if($sekolahedit->logo == null)				
		{{ Form::label('logo', 'Logo Belum Ada Tolong diisi:') }}		
		@else		
		{{ Form::label('logo', 'Logo Sudah Ada(lewati field ini jika anda tidak ingin mengganti logo):') }}
		@endif

		{{ Form::file('logo', null, ['placeholder' => 'Logo(Leave this blank)', 'class' => 'form-control'])}}
		{{ errors_for('logo', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('alamat', 'Alamat:') }}
		{{ Form::text('alamat', null, ['class' => 'form-control'])}}
		{{ errors_for('alamat', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('telepon', 'Telepon:') }}
		{{ Form::text('telepon', null, ['class' => 'form-control'])}}
		{{ errors_for('telepon', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('akreditasi', 'Akreditasi Sekolah:') }}
		{{ Form::text('akreditasi', null, ['class' => 'form-control'])}}
		{{ errors_for('akreditasi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('jml_pengajar', 'Jumlah Pengajar:') }}
		{{ Form::text('jml_pengajar', null, ['class' => 'form-control'])}}
		{{ errors_for('jml_pengajar', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('deskripsi', 'Deskripsi:')}}
		{{ Form::text('deskripsi', null, ['class' => 'form-control'])}}
		{{ errors_for('deskripsi', $errors) }}
	</div>
	<div class="form-group">
		{{ Form::label('aktif', 'Aktivasi Sekolah:')}}
		{{ Form::select('aktif', array('0' => 'Sekolah Tidak Aktif', '1' => 'Sekolah Aktif'), null, ['class' => 'form-control']) }}
		{{ errors_for('aktif', $errors)}}
	</div>
	<!-- Update Profile Field -->
	<div class="form-group">
		{{ Form::submit('Update Sekolah', ['class' => 'btn btn-primary']) }}{{ HTML::link('admin/sekolah/'.$sekolahedit->id, 'Back') }}
	</div>

	{{ Form::close() }}

@stop