@extends('protected.admin.master')

@section('title', 'Edit Lowongan Kerja')

@section('content')
	<h1>Edit Lowongan Kerja ({{ $lokeredit->instansi }} - {{$lokeredit->posisi}})</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($lokeredit, ['method' => 'PATCH', 'route' => ['admin.loker.update', $lokeredit->id]]) }}

	

	<div class="form-group">
		{{ Form::label('sekolah_id', 'Sekolah:')}}
		{{ Form::select('sekolah_id', $sekolahs, Input::old('sekolah_id'), ['class' => 'form-control'])}}
		{{ errors_for('sekolah_id', $errors) }}
	</div>


	<div class="form-group">
		{{ Form::label('link', 'Link:') }}
		{{ Form::text('link', null, ['class' => 'form-control']) }}
		{{ errors_for('link', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('instansi', 'Instansi:') }}
		{{ Form::text('instansi', null, ['class' => 'form-control']) }}
		{{ errors_for('instansi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('posisi', 'Posisi:') }}
		{{ Form::text('posisi', null, ['class' => 'form-control']) }}
		{{ errors_for('posisi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('deskripsi', 'Deskripsi:') }}
		{{ Form::text('deskripsi', null, ['class' => 'form-control']) }}
		{{ errors_for('deskripsi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('aktifform', 'Status Aktivasi:')}}
		{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), array('1'), ['class' => 'form-control']) }}
		{{ errors_for('aktifform', $errors)}}
	</div>


	<!-- Update Profile Field -->
	<div class="form-group">
		{{ Form::submit('Update Lowongan Kerja', ['class' => 'btn btn-primary']) }}{{ HTML::link('admin/loker/'.$lokeredit->sekolah_id, 'Back') }}
	</div>

	{{ Form::close() }}

@stop