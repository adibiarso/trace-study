@extends('protected.admin.master')

@section('title', 'List Lowongan Kerja')

@section('content')
	<h1>{{ $datasekolah->nama }}</h1>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>ID</th>
	          <th>Link</th>
			  <th>Instansi Loker</th>
			  <th>Posisi</th>
			  <th>Deskripsi</th>
			  <th>Dibuat</th>
			  <th>Diupdate</th>
			  <th>Aktivasi</th>
			  <th>Aksi</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($listloker as $loker)
      		<tr>
      			<td>{{ $loker->id}}</td>
	      		<td><a href="{{ $loker->link }}">Link</a></td>
		        <td>{{ $loker->instansi}} </td>
				<td>{{ $loker->posisi }}</td>
				<td>{{ $loker->deskripsi }}</td>
				<td>{{{ date('d-m-Y H:i:s', strtotime($loker->created_at)) }}}</td>
				<td>{{{ date('d-m-Y H:i:s', strtotime($loker->updated_at)) }}}</td>
				<td>
					@if($loker->aktif == 1)
						{{"Loker Aktif"}}
					@else
						{{"Loker Tidak Aktif"}}
					@endif
				</td> 
				<td>{{ link_to_route('admin.loker.edit', 'Edit', $loker->id, ['class' => 'btn btn-primary']) }}
					{{ link_to_route('admin.loker.detail', 'Details', $loker->id, ['class' => 'btn btn-primary']) }}
				</td>
		     </tr>
			@endforeach
      	</tbody>
	</table>
	@if(Sentry::check())

		{{ link_to_route('admin.loker.create', 'Tambah', $datasekolah->id, ['class' => 'btn btn-primary']) }}
					
	@endif
	{{ HTML::link('admin/loker', 'Back', ['class' => 'btn btn-primary']) }}
@stop