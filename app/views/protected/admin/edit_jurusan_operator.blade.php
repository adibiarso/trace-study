@extends('protected.admin.master')

@section('title', 'Edit Operator')

@section('content')
	<h1>Edit Operator</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($operatoredit, ['method' => 'PATCH', 'route' => ['admin.jurusan.updateoperator', $operatoredit->id]]) }}
		
		<!--<div class="form-group">
			 Form::label('sekolah_id', 'Sekolah:')
			 Form::select('sekolah_id', $sekolahs, Input::old('sekolah_id'), ['class' => 'form-control'])
			 errors_for('sekolah_id', $errors) 
		</div>--> <!-- gabisa ganti sekolah, karena pake pivot -->
		<div><input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"></div>

		<div class="form-group">
			{{ Form::label('email', 'Email:')}}
			{{ Form::text('email', $operatoredit->email, ['class' => 'form-control'])}}
			{{ errors_for('email', $errors) }}
		</div>

		<div class="form-group">
			{{ Form::label('username', 'Username:')}}
			{{ Form::text('username', $operatoredit->username, ['class' => 'form-control'])}}
			{{ errors_for('username', $errors) }}
		</div>

		<!-- First name field -->
		<div class="form-group">
			{{ Form::label('first_name', 'First Name:')}}
			{{ Form::text('first_name', $operatoredit->first_name, ['class' => 'form-control'])}}
			{{ errors_for('first_name', $errors) }}
		</div>

		<!-- Last name field -->
		<div class="form-group">
			{{ Form::label('last_name', 'Last Name:')}}
			{{ Form::text('last_name', $operatoredit->last_name, ['class' => 'form-control'])}}
			{{ errors_for('last_name', $errors) }}
		</div>
		<!-- END TABEL USERS -->

		<div class="form-group">
			{{ Form::label('aktifform', 'Status Aktivasi:')}}
			{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), $operatoredit->activated, ['class' => 'form-control']) }}
			{{ errors_for('aktifform', $errors)}}
		</div>

		<!-- Update Profile Field -->
		<div class="form-group">
			{{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
		</div>
	{{ Form::close() }}

@stop