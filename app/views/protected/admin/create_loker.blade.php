@extends('protected.admin.master')

@section('title', 'Tambah Lowongan Kerja')

@section('content')


		<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Tambah Lowongan Kerja - {{$datasekolah->nama}}</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['method' => 'post', 'route' => ['admin.loker.store', $datasekolah->id]]) }}
	                    <fieldset>
	                    	@if (Session::has('flash_message'))
								<div class="form-group">
									<p>{{ Session::get('flash_message') }}</p>
								</div>
							@endif

							<div class="form-group">
								{{ Form::text('link', null, ['placeholder' => 'Link Web', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('link', $errors) }}
							</div>
							<div class="form-group">
								{{ Form::text('instansi', null, ['placeholder' => 'Instansi', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('instansi', $errors) }}
							</div>
							<div class="form-group">
								{{ Form::text('posisi', null, ['placeholder' => 'Posisi', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('posisi', $errors) }}
							</div>
							<div class="form-group">
								{{ Form::textarea('deskripsi', null, ['placeholder' => 'Deskripsi', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('deskripsi', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::label('aktifform', 'Status Aktivasi:')}}
								{{ Form::select('aktifform', array('0' => 'Tidak Aktif', '1' => 'Aktif'), array('1'), ['class' => 'form-control']) }}
								{{ errors_for('aktifform', $errors)}}
							</div>

							<!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Tambah', ['class' => 'btn btn-lg btn-primary btn-block']) }}
								{{ HTML::link('admin/loker/'.$datasekolah->id, 'Back') }}
							</div>
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
			</div>
		</div>
	</div>



@stop