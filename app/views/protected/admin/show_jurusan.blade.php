@extends('protected.admin.master')

@section('title', 'View Jurusan')

@section('content')
	<h1>{{ $listjurusan->sekolah->nama }}</h1>
	<h2>{{ $listjurusan->nama }}'s Profile</h2>
	<ul>
		<li>ID: {{$listjurusan->id}}</li>
		<li>Nama: {{ $listjurusan->nama }}</li>
		<li>Created By: {{ $listjurusan->jurusancreatedby->username }}</li>
		<li>Last Modified By: {{$listjurusan->jurusanmodifiedby->username}}</li>
		<li>Aktivasi: 
			@if($listjurusan->aktif == 1)
				{{"Jurusan Aktif"}}
			@else
				{{"Jurusan Tidak Aktif"}}
			@endif
		</li> 
	</ul>

	@if(Sentry::check())

		{{ link_to_route('admin.jurusan.edit', 'Edit', $listjurusan->id, ['class' => 'btn btn-primary']) }}
					
	@endif
	{{ HTML::link('admin/jurusan/'.$listjurusan->sekolah->id, 'Back') }}

@stop