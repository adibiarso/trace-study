@extends('protected.operator.master')

@section('title', 'List Users')

@section('content')

<h2>Pending User Approval</h2>
	
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>id</th>
	          <th>Email</th>
	          <th>First Name</th>
	          <th>Last Name</th>
              <th>Jurusan</th>
              <th>Tahun</th>
              
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($users as $user)
      		<tr>
	      		<td>{{ $user->alumni_id }}</td>
		        <td>{{ link_to_route('operator.profiles.show', $user->email, $user->id) }}<br>		        
		        </td>
		        <td>{{ $user->first_name}}</td>
		        <td>{{ $user->last_name}}</td>
                <td>{{ $user->nama }}</td>
                <td>{{ $user->recruitmentsekolah }}</td>
                
                <td>{{ HTML::link('operator/approval/'.$user->alumni_id.'/'.$user->id, 'Approve', ['class' => 'btn btn-sm btn-info']) }}
                {{ HTML::link('operator/decline/'.$user->alumni_id.'/'.$user->id, 'Decline', ['class' => 'btn btn-sm btn-danger']) }}
                </td>
		     </tr>
			@endforeach

      	</tbody>
	</table>

@stop