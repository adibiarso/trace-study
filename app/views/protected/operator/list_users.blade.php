@extends('protected.operator.master')

@section('title', 'List Users')

@section('content')

<h2>Registered Users</h2>
	<p>Here you would normally search for users but since this is just a demo, I'm listing all of them.</p>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>id</th>
	          <th>Email</th>
	          <th>First Name</th>
	          <th>Last Name</th>
              <th>Jurusan</th>
              <th>Tahun</th>
              <th>Minat Wirausaha</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($users as $user)
      		<tr>
	      		<td>{{ $user->alumni_id }}</td>
		        <td>{{ link_to_route('operator.profiles.show', $user->email, $user->id) }}<br>		        
		        </td>
		        <td>{{ $user->first_name}}</td>
		        <td>{{ $user->last_name}}</td>
                <td>{{ $user->nama }}</td>
                <td>{{ $user->recruitmentsekolah }}</td>
                <td>{{ $user->minatwirausaha }}</td>
                <td>{{ HTML::link('operator/decline/'.$user->alumni_id.'/'.$user->id, 'Remove', ['class' => 'btn btn-sm btn-danger']) }}</td>
		     </tr>
			@endforeach

      	</tbody>
	</table>

@stop