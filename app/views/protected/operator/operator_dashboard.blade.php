@extends('protected.operator.master')

@section('title', 'Operator Dashboard')

@section('content')

	@if (Session::has('flash_message'))
			<p>{{ Session::get('flash_message') }}</p>
	@endif


	<div class="jumbotron">
		<h1>Operator Page</h1>
		<p>This page is for operators only!</p>
	</div>


@stop