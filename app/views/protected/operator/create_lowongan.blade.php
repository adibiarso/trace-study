@extends('protected.operator.master')

@section('title', 'Tambahkan Lowongan Baru')

@section('content')


		<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Tambah Lowongan</h3>
				 	</div>
				  	<div class="panel-body">
				    	{{ Form::open(['route' => 'operator.lowongan.store']) }}
	                    <fieldset>
	                    	@if (Session::has('flash_message'))
								<div class="form-group">
									<p>{{ Session::get('flash_message') }}</p>
								</div>
							@endif

							<div class="form-group">
								{{ Form::text('instansi', null, ['placeholder' => 'Nama Instansi', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('nama', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('posisi', null, ['placeholder' => 'Posisi', 'class' => 'form-control', 'required' => 'required'])}}
								{{ errors_for('alamat', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::textarea('deskripsi', null, ['placeholder' => 'Deskripsi Pekerjaan', 'class' => 'form-control', 'required' => 'required', 'size' =>'1x5'])}}
								{{ errors_for('telepon', $errors) }}
							</div>

							<div class="form-group">
								{{ Form::text('link', null, ['placeholder' => 'http:\\contoh.com\lowongan\kerja', 'class' => 'form-control'])}}
								{{ errors_for('akreditasi', $errors) }}
							</div>
                            
                            <!-- Submit field -->
							<div class="form-group">
								{{ Form::submit('Tambah', ['class' => 'btn btn-lg btn-primary btn-block']) }}
								{{ HTML::link('operator/lowongan', 'Back',['class' => 'btn btn-lg btn-info btn-block']) }}
							</div>
                            
				    	</fieldset>
				      	{{ Form::close() }}
				    </div>
				</div>
			</div>
		</div>
	</div>



@stop