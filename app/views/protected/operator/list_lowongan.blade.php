@extends('protected.operator.master')

@section('title', 'List Lowongan Pekerjaan')

@section('content')

<h2>Daftar Lowongan</h2>
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	          <th>Instansi</th>
			  <th>Posisi</th>
			  <th>Deskripsi</th>
              <th>Link</th>
			  <th>Status</th>
              <th>Aksi</th>
			  <!-- <th>Aksi</th> -->
      	</thead>

      	<tbody>
      		@foreach ($listlowongan as $lowongan)
      		<tr>
	      		<td>{{ $lowongan->instansi }}<br>
		        <td>{{ $lowongan->posisi }}</td>
				<td>{{ $lowongan->deskripsi }}</td>
                
                <td>@if(! $lowongan->link == NULL)
                {{ HTML::link($lowongan->link) }}
                @else
                Tidak Ada Link
                @endif
                
                </td>
                
				<td>
					@if($lowongan->aktif == 1)
						{{"Lowongan Aktif"}}
					@else
						{{"Lowongan Tidak Aktif"}}
					@endif
				</td>
				
                <td>
                {{ link_to_route('operator.lowongan.edit', 'Edit', $lowongan->id, ['class' => 'btn btn-sm btn-primary']) }}
                {{ HTML::link('operator/lowongan/deactive/'.$lowongan->id, 'Deactive', ['class' => 'btn btn-sm btn-danger']) }}
                </td>
                
		     </tr>
			@endforeach
      	</tbody>
	</table>
    {{ link_to_route('operator.lowongan.create', 'ADD', null, ['class' => 'btn btn-primary']) }}

@stop