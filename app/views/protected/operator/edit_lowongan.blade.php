@extends('protected.admin.master')

@section('title', 'Edit Lowongan')

@section('content')
<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Edit Lowongan</h3>
				 	</div>
				  	<div class="panel-body">
                    
        @if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif

	{{ Form::model($lowongan, ['method' => 'PATCH', 'route' => ['operator.lowongan.update', $lowongan->id]]) }}

	<div class="form-group">
		{{ Form::label('instansi', 'Instansi:') }}
		{{ Form::text('instansi', null, ['class' => 'form-control']) }}
		{{ errors_for('instansi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('posisi', 'Posisi:') }}
		{{ Form::text('posisi', null, ['class' => 'form-control'])}}
		{{ errors_for('posisi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('deskripsi', 'Deskripsi:') }}
		{{ Form::textarea('deskripsi', null, ['class' => 'form-control'])}}
		{{ errors_for('deskripsi', $errors) }}
	</div>

	<div class="form-group">
		{{ Form::label('link', 'Link:') }}
		{{ Form::text('link', null, ['class' => 'form-control'])}}
		{{ errors_for('link', $errors) }}
	</div>

	<!-- Update Profile Field -->
	<div class="form-group">
		{{ Form::submit('Update Lowongan', ['class' => 'btn btn-primary']) }}
        {{ HTML::link('operator/lowongan', 'Back',['class' => 'btn btn-info']) }}
	</div>

	{{ Form::close() }}
    </div>
				</div>
			</div>
		</div>
	</div>

@stop