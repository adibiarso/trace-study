@extends('master')

@section('title', 'Profile')

@section('content')
<h4>Akun</h4>
<table class="table table-striped table-bordered table-hover">
	<tr>
	<td>Nama  </td>
	<td>{{ $user->first_name }} {{ $user->last_name }}</td>
	</tr>
		
	<tr>
	<td>Email  </td>
	<td>{{ $user->email }}</td>
	</tr>
</table>
@if(Sentry::check())

		{{ link_to_route('profiles.edit', 'Edit Akun', $user->id, ['class' => 'btn btn-primary']) }}

	@endif
<h4>Biodata</h4>
<table class="table table-striped table-bordered table-hover">
	@foreach ($listalumni as $alumni)
      		<tr>
	      		
		        <td>NIS</td>
		        <td>{{$alumni->nis}}</td>
		        
		     </tr>

		     <tr>
	      		
		        <td>Tanggal Lahir</td>
		        <td>{{$alumni->tanggallahir}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Tempat Lahir</td>
		        <td>{{$alumni->tempatlahir}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Gender</td>
		        <td>{{$alumni->gender}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>No HP</td>
		        <td>{{$alumni->nohp}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Alamat</td>
		        <td>{{$alumni->alamat}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Telepon</td>
		        <td>{{$alumni->telepon}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Ikut Recruitment Sekolah</td>
		        <td>{{$alumni->recruitmentsekolah}}</td>
		        
		     </tr>
		     <tr>
	      		
		        <td>Minat Wirausaha</td>
		        <td>{{$alumni->minatwirausaha}}</td>
		        
		     </tr>
	@endforeach
		
	
</table>	
<h4>Riwayat Pekerjaan</h4>
<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	         
	          <th>Instansi</th>
	          <th>Posisi</th>
	          <th>Tahun</th>
	          <th>Jenis</th>
	          <th>Sesuai Bidang Keahlian</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($listpekerjaan as $pekerjaan)
      		<tr>
	      		
		        <td>{{ $pekerjaan->instansi}}</td>
		        <td>{{ $pekerjaan->posisi}}</td>
		        <td>{{ $pekerjaan->tahunmasuk}} - {{ $pekerjaan->tahunkeluar}}</td>
		        <td>{{ $pekerjaan->jenis}}</td>
		        <td>{{ $pekerjaan->sesuai}}</td>
		     </tr>
			@endforeach

      	</tbody>
</table>
<h4>Ekstrakurikuler</h4>
<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	         
	          <th>Ekstrakurikuler</th>
	          <th>Jabatan</th>
	          <th>Tahun</th>
	          </tr>
      	</thead>

      	<tbody>
      		@foreach ($listekskul as $ekskul)
      		<tr>
	      		
		        <td>{{ $ekskul->nama}}</td>
		        <td>{{ $ekskul->jabatan}}</td>
		        <td>{{ $ekskul->tahunmasuk}} - {{ $ekskul->tahunkeluar}}</td>
		       
		     </tr>
			@endforeach

      	</tbody>
</table>
<h4>Pelatihan</h4>
<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	         
	          <th>Pelatihan</th>
	          <th>Penyelenggara</th>
	          <th>Tahun</th>
	          </tr>
      	</thead>

      	<tbody>
      		@foreach ($listtraining as $training)
      		<tr>
	      		
		        <td>{{ $training->nama}}</td>
		        <td>{{ $training->penyelenggara}}</td>
		        <td>{{ $training->tahun}}</td>
		       
		     </tr>
			@endforeach

      	</tbody>
</table>


		{{ HTML::link('profiles/'.Sentry::getUser()->id.'/editb', 'Edit Biodata', ['class' => 'btn btn-primary']) }}


@stop