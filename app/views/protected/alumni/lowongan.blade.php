@extends('master')

@section('title', 'List Lowongan Pekerjaan')

@section('content')
@if (Session::has('flash_message'))
			<p>{{ Session::get('flash_message') }}</p>
@endif

@if (Sentry::check())

<h2>Daftar Lowongan Pekerjaan</h2>
	
	<table class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	         
	          <th>Instansi</th>
	          <th>Posisi</th>
	          <th>Deskripsi</th>
	        </tr>
      	</thead>

      	<tbody>
      		@foreach ($listlowongan as $lowongan)
      		<tr>
	      		
		        <td>{{ $lowongan->instansi}}</td>
		        <td>{{ $lowongan->posisi}}</td>
		        <td>{{ $lowongan->deskripsi}}</td>
		     </tr>
			@endforeach

      	</tbody>
	</table>
@endif
@stop