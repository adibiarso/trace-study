@extends('master')

@section('title', 'Edit Profile')

@section('content')
	<h1>Edit Profile</h1>

	@if (Session::has('flash_message'))
		<div class="form-group">
			<p style="padding: 5px" class="bg-success">{{ Session::get('flash_message') }}</p>
		</div>
	@endif


	{{ Form::model($user, ['id'=>'multi' ,'method' => 'PATCH', 'route' => ['updateb', $user->id]]) }}
	{{ Form::hidden('jmlp', 0) }}
	{{ Form::hidden('jmle', 0) }}
	{{ Form::hidden('jmlpl', 0) }}


		<!-- email Field -->
		<div class="form-group">
			{{ Form::label('nis', 'NIS :') }}
			{{ Form::text('nis', null, ['class' => 'form-control']) }}
			{{ errors_for('nis', $errors) }}
		</div>


		<!-- first_name Field -->
		<div class="form-group">
			{{ Form::label('tempatlahir', 'Tempat Lahir:') }}
			{{ Form::text('tempatlahir', null, ['class' => 'form-control']) }}
			{{ errors_for('tempatlahir', $errors) }}
		</div>

		<!-- last_name Field -->
		<div class="form-group">
			{{ Form::label('tanggallahir', 'Tanggal Lahir :') }}
			{{ Form::text('tanggallahir', null, ['class' => 'form-control']) }}
			{{ errors_for('tanggallahir', $errors) }}

		</div>

		<!-- Password field -->
		<div class="form-group">
			{{ Form::label('gender', 'Jenis Kelamin :') }}
			{{ Form::text('gender', null, ['class' => 'form-control']) }}
			{{ errors_for('gender', $errors) }}

		</div>

		<!-- Password Confirmation field -->
		<div class="form-group">
			{{ Form::label('nohp', 'Nomor Handphone :') }}
			{{ Form::text('nohp', null, ['class' => 'form-control']) }}
			{{ errors_for('nohp', $errors) }}

		</div>

		<div class="form-group">
			{{ Form::label('alamat', 'Alamat :') }}
			{{ Form::text('alamat', null, ['class' => 'form-control']) }}
			{{ errors_for('alamat', $errors) }}

		</div>

		<div class="form-group">
			{{ Form::label('recruitmentsekolah', 'Recruitment Sekolah :') }}
			{{ Form::text('recruitmentsekolah', null, ['class' => 'form-control']) }}
			{{ errors_for('recruitmentsekolah', $errors) }}

		</div>

		<div class="form-group">
			{{ Form::label('minatwirausaha', 'Berminat Wirausaha :') }}
			{{ Form::text('minatwirausaha', null, ['class' => 'form-control']) }}
			{{ errors_for('minatwirausaha', $errors) }}

		</div>

		<!-- Update Profile Field -->
		
	<h4>Riwayat Pekerjaan</h4>
	<table id="table2" class="table table-striped table-bordered table-hover">
		<thead>
	        <tr>
	         
	          <th>Instansi</th>
	          <th>Posisi</th>
	          <th>Tahun</th>
	          <th>Jenis</th>
	          <th>Sesuai Bidang Keahlian</th>
	          <th>Aksi</th>
	        </tr>
      	</thead>

      	<tbody>
      	<tr class="tr_clone"></tr>
      <?php
$l = 0;
$m = 0;
$n = 0;
      ?>
{{ Form::hidden('jmll', $l) }}
	      		@foreach ($listpekerjaan as $pekerjaan)
	      		  <?php $l++; ?>
	      		{{ Form::hidden('pid'.$l, $pekerjaan->id) }}
	      		{{ Form::hidden('jmll', $l) }}
	      		<tr class="tr_clone">
		      		{{ Form::hidden('deletep'.$l, 0, ['class' => 'deletep']) }}
			        <td>{{ Form::text('pinstansi'.$l, $pekerjaan->instansi, ['class' => 'form-control']) }}</td>
			        <td>{{ Form::text('pposisi'.$l, $pekerjaan->posisi, ['class' => 'form-control']) }}</td>
			        <td>{{ Form::text('ptahunmasuk'.$l, $pekerjaan->tahunmasuk, ['class' => 'form-control','style' => 'width:100px; float:left;']) }} <div style="float:left;">-</div> {{ Form::text('ptahunkeluar'.$l, $pekerjaan->tahunkeluar, ['class' => 'form-control','style' => 'width:100px']) }}</td>
			        <td>{{ Form::text('pjenis'.$l, $pekerjaan->jenis, ['class' => 'form-control']) }}</td>
			        <td>{{ Form::text('psesuai'.$l, $pekerjaan->sesuai, ['class' => 'form-control']) }}</td>
			        <td><button type="button" class="btn btn-danger btn-sm" id="delete1">Delete</button></td>
			     </tr>
			   
				@endforeach
			
			
      	</tbody>
</table>
<button type="button" class="btn btn-default btn-sm pull-right" id="click">
  <span class="glyphicon glyphicon-plus"></span> Tambah
</button>
<br><br>
<h4>Ekstrakurikuler</h4>
<table class="table table-striped table-bordered table-hover" id="table3">
		<thead>
	        <tr>
	         
	          <th>Ekstrakurikuler</th>
	          <th>Jabatan</th>
	          <th>Tahun</th>
	          <th>Aksi</th>
	          </tr>
      	</thead>

      	<tbody>
<tr class="tr_clone1"></tr>
      	{{ Form::hidden('jmlm', $m) }}

      		@foreach ($listekskul as $ekskul)
      		  <?php $m++; ?>
      		{{ Form::hidden('deletee'.$m, 0) }}
      		{{ Form::hidden('jmlm', $m) }}

      		<tr  class="tr_clone1">
      		{{ Form::hidden('deletee'.$m, 0, ['class' => 'deletee']) }}
	      		{{ Form::hidden('eid'.$m, $ekskul->id) }}
		        <td>{{ Form::text('enama'.$m, $ekskul->nama, ['class' => 'form-control']) }}</td>
		        <td>{{ Form::text('ejabatan'.$m, $ekskul->jabatan, ['class' => 'form-control']) }}</td>
		        <td>{{ Form::text('etahunmasuk'.$m, $ekskul->tahunmasuk, ['class' => 'form-control','style' => 'width:100px; float:left;']) }} <div style="float:left;">-</div> {{ Form::text('etahunkeluar'.$m, $ekskul->tahunkeluar, ['class' => 'form-control','style' => 'width:100px']) }}</td>
		        <td><button type="button" class="btn btn-danger btn-sm" id="delete2">Delete</button></td>
		     </tr>
		    
			@endforeach
      	</tbody>
</table>	
<button type="button" class="btn btn-default btn-sm pull-right" id="click1">
  <span class="glyphicon glyphicon-plus"></span> Tambah
</button>
<br><br>
<h4>Pelatihan</h4>
<table class="table table-striped table-bordered table-hover" id="table4">
		<thead>
	        <tr>
	         
	          <th>Pelatihan</th>
	          <th>Penyelenggara</th>
	          <th>Tahun</th>
	          <th>Aksi</th>
	          </tr>
      	</thead>

      	<tbody>
      	<tr class="tr_clone2"></tr>
      	{{ Form::hidden('jmln', $n) }}
      		@foreach ($listtraining as $training)
      		 <?php $n++; ?>
      		{{ Form::hidden('deletepl'.$n, 0) }}
      		{{ Form::hidden('jmln', $n) }}
      		<tr class="tr_clone2">
      		{{ Form::hidden('deletepl'.$n, 0, ['class' => 'deletepl']) }}
	      		{{ Form::hidden('plid'.$n, $training->id) }}
		        <td>{{ Form::text('plnama'.$n, $training->nama, ['class' => 'form-control']) }}</td>
		        <td>{{ Form::text('plpenyelenggara'.$n, $training->penyelenggara, ['class' => 'form-control']) }}</td>
		        <td>{{ Form::text('pltahun'.$n, $training->tahun, ['class' => 'form-control']) }}</td>
		        <td><button type="button" class="btn btn-danger btn-sm" id="delete3">Delete</button></td>
		     </tr>
		     
			@endforeach

      	</tbody>
</table>		
<button type="button" class="btn btn-default btn-sm pull-right" id="click2">
  <span class="glyphicon glyphicon-plus"></span> Tambah
</button>
<br><br>
<div class="form-group">
			{{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
		</div>
	
	{{ Form::close() }}
<br><br>
@stop