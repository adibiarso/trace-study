@extends('master')

@section('title', 'Sekolah')

@section('content')
@if (Session::has('flash_message'))
			<p>{{ Session::get('flash_message') }}</p>
@endif

@if (Sentry::check())
<div class="jumbotron">
<h2>Profile Sekolah</h2>
	
	 @foreach ($listsekolah as $sekolah)
      		<h3>{{ $sekolah->nama}}</h3>
      		<p>Alamat : {{ $sekolah->alamat}}</p>
      		<p>No Telp : {{ $sekolah->telepon}}</p>
      		<p>Akreditasi : {{ $sekolah->akreditasi}}</p>
      		<p>Jumlah Pengajar : {{ $sekolah->jml_pengajar}}</p>
      		<p>Deskripsi : {{ $sekolah->deskripsi}}</p>
	@endforeach
</div>
@endif
@stop