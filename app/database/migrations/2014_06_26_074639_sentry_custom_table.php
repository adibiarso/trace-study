<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SentryCustomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
        {
            $table->string('username')->unique();
        });
		
		Schema::table('users_groups', function($table) 
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
		});

        Schema::create('mastersekolah', function($table)
		{
			$table->increments('id');
			$table->string('nama', 50);
			$table->string('logo', 20)->nullable();
			$table->string('alamat');
			$table->string('telepon',20);//tambah
			$table->string('akreditasi',20);//tambah
			$table->integer('jml_pengajar');//tambah
			$table->text('deskripsi');//tambah
			$table->integer('createdby')->unsigned();
			$table->integer('lastmodifiedby')->unsigned();
			$table->integer('aktif');
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->foreign('createdby')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('lastmodifiedby')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::create('users_sekolah', function($table)
		{
			$table->integer('sekolah_id')->unsigned(); //id sekolah
			$table->integer('operator_id')->unsigned(); //id user bertindak operator
			$table->engine = 'InnoDB';
			$table->primary(array('sekolah_id', 'operator_id'));
			$table->foreign('sekolah_id')->references('id')->on('mastersekolah')->onDelete('cascade');
			$table->foreign('operator_id')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::create('jurusan', function($table)
		{
			$table->increments('id');
			$table->integer('sekolah_id')->unsigned();
			$table->string('nama');
			$table->integer('createdby')->unsigned();
			$table->integer('lastmodifiedby')->unsigned();
			$table->integer('aktif');
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->foreign('sekolah_id')->references('id')->on('mastersekolah')->onDelete('cascade');
			$table->foreign('createdby')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('lastmodifiedby')->references('id')->on('users')->onDelete('cascade');
		});

		/*Schema::create('jurusan_sekolah', function($table)
		{
			$table->integer('sekolah_id')->unsigned(); //id sekolah
			$table->integer('jurusan_id')->unsigned(); //id jurusan yang berada di sekolah tsb
			$table->engine = 'InnoDB';
			$table->primary(array('sekolah_id', 'jurusan_id'));
		});*/

		Schema::create('alumni', function($table)
		{
			$table->increments('id');
			$table->Integer('user_id')->unsigned()->unique();
			$table->string('nis',20)->unique()->nullable();;
			$table->date('tanggallahir');
			$table->string('tempatlahir');
			$table->string('gender');
			$table->string('fb',30);
			$table->string('nohp',20);
			$table->text('alamat');
			$table->string('telepon',20);
			$table->integer('jurusan_id')->unsigned();
			$table->integer('recruitmentsekolah');
			$table->integer('statusbekerja');
			$table->text('kegiatankelompok');
			$table->integer('createdby')->unsigned();
			$table->integer('approval')->unsigned();
			$table->integer('lastmodifiedby')->unsigned();	
			$table->string('minatwirausaha');//tambah		
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('jurusan_id')->references('id')->on('jurusan')->onDelete('cascade');
			$table->foreign('createdby')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('lastmodifiedby')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::create('lowongankerja', function($table)
		{
			$table->increments('id');
			$table->integer('sekolah_id')->unsigned();
			$table->string('link');
			$table->string('instansi');
			$table->string('posisi');
			$table->text('deskripsi');			
			$table->integer('createdby')->unsigned();
			$table->integer('lastmodifiedby')->unsigned();			
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->integer('aktif');
			$table->foreign('createdby')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('sekolah_id')->references('id')->on('mastersekolah')->onDelete('cascade');
			$table->foreign('lastmodifiedby')->references('id')->on('users')->onDelete('cascade');
		});

		Schema::create('training', function($table)
		{
			$table->increments('id');			
			$table->integer('alumni_id')->unsigned();
			$table->string('nama');
			$table->integer('tahun');
			$table->string('penyelenggara');
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->integer('aktif');
			$table->foreign('alumni_id')->references('id')->on('alumni')->onDelete('cascade');
		});

		Schema::create('ekskul', function($table)
		{
			$table->increments('id');			
			$table->integer('alumni_id')->unsigned();
			$table->string('nama');
			$table->integer('tahunmasuk');
			$table->integer('tahunkeluar');
			$table->string('jabatan');
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->integer('aktif');
			$table->foreign('alumni_id')->references('id')->on('alumni')->onDelete('cascade');
		});

		Schema::create('pengalamankerja', function($table)
		{
			$table->increments('id');			
			$table->integer('alumni_id')->unsigned();
			$table->string('instansi');
			$table->string('posisi');
			$table->integer('sesuai');
			$table->integer('tahunmasuk');
			$table->integer('tahunkeluar');
			$table->string('jenis');
			$table->timestamps();//created_at, updated_at
			$table->engine = 'InnoDB';
			$table->integer('aktif');
			$table->foreign('alumni_id')->references('id')->on('alumni')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pengalamankerja');
		Schema::drop('ekskul');
		Schema::drop('training');
		Schema::drop('lowongankerja');

		Schema::drop('users_sekolah');
		
		Schema::drop('alumni');

		//Schema::drop('jurusan_sekolah');

		Schema::drop('jurusan');

		
		Schema::drop('mastersekolah');
		
		Schema::table('users', function($table)
        {
            $table->dropColumn('username');
        });
	}

}
