<?php

class SentryUserGroupSeeder extends Seeder {

	public function run()
	{
		DB::table('users_groups')->delete();

		$adminUser  = Sentry::getUserProvider()->findByLogin('admin@admin.com');
		$operatorUser  = Sentry::getUserProvider()->findByLogin('operator@operator.com');
		$alumniUser  = Sentry::getUserProvider()->findByLogin('alumni@alumni.com');
		
		$adminGroup = Sentry::getGroupProvider()->findByName('Admin');
		$operatorGroup = Sentry::getGroupProvider()->findByName('Operator');
		$alumniGroup = Sentry::getGroupProvider()->findByName('Alumni');

		$adminUser->addGroup($adminGroup);
		$operatorUser->addGroup($operatorGroup);
		$alumniUser->addGroup($alumniGroup);
	}
}