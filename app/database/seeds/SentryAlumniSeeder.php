<?php

class SentryAlumniSeeder extends Seeder {

	public function run()
	{
		$alumni = array(
				'user_id'				=> 3,
				'nis' 					=> '10001',
				'tanggallahir'			=> '1994-02-28', 
				'tempatlahir' 			=> 'Jatinangor',
				'gender' 				=> 'L',
				'fb' 					=> 'bojez.berdarah',
				'nohp' 					=> '085666768696',
				'alamat' 				=> 'Bekasi Indah Square',
				'telepon' 				=> '(021)123456',
				'jurusan_id' 			=> 1,
				'approval' 			=> 1,
				'recruitmentsekolah' 	=> 2009,
				'statusbekerja' 		=> 1,
				'kegiatankelompok' 		=> 'basket',
				'minatwirausaha'		=> 'minat',
				'createdby' 			=> 2,
				'lastmodifiedby' 		=> 2,
				'created_at' 			=> new DateTime,
				'updated_at' 			=> new DateTime
			);

			DB::table('alumni')->insert($alumni);
	}
}
