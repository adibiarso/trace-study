<?php

class KegiatanAlumniSeeder extends Seeder {

	public function run()
	{
		$lowongan = array(
				'sekolah_id' => 1,
				'link' => 'Http://google.com',				
				'instansi' => 'Google',
				'posisi' => 'CEO',
				'deskripsi' => 'Lowongan Kerja Untuk SMK 1 Kota Bekasi, menjadi CEO di Google.',
				'createdby' => 1, 
				'lastmodifiedby' => 1,
				'aktif' => 1,
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('lowongankerja')->insert($lowongan);

		$training = array(
				'alumni_id' => 1,
				'nama' => 'Pelatihan Microsoft Office',
				'tahun' => 2011,
				'aktif' => 1,
				'penyelenggara' => 'Microsoft Indonesia',
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('training')->insert($training);

		$ekskul = array(
				'alumni_id' => 1,
				'nama' => 'Merpati Putih',
				'tahunmasuk' => 2010,
				'tahunkeluar' => 2012,
				'aktif' => 1,
				'jabatan' => 'Ketua Merpati Putih Periode 2010-2011',
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('ekskul')->insert($ekskul);

		$pengalamankerja = array(
				'alumni_id' => 1,
				'instansi' => 'Google',
				'posisi' => 'CEO',
				'sesuai' => 1,				
				'tahunmasuk' => 2013,
				'tahunkeluar' => 2014,	
				'jenis' => 'kerja',
				'aktif' => 1,
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('pengalamankerja')->insert($pengalamankerja);
	}
}