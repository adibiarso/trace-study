<?php

class SentryJurusanSeeder extends Seeder {

	public function run()
	{
		$jurusan = array(
				'nama' => 'Rekayasa Perangkat Lunak',
				'sekolah_id' => 1,
				'createdby' => 2, 
				'lastmodifiedby' => 2,
				'aktif' => 1,
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('jurusan')->insert($jurusan);
	}
}
