<?php

class SentryJurusanSekolahSeeder extends Seeder {

	public function run()
	{
		$jurusan_sekolah = array(
				'sekolah_id' => 1,
				'jurusan_id' => 1
			);

		DB::table('jurusan_sekolah')->insert($jurusan_sekolah);
	}
}