<?php

class SentryOperatorSekolahSeeder extends Seeder {

	public function run()
	{
		$operatorsekolah = array(
				'sekolah_id' => 1,
				'operator_id' => 2
			);

		DB::table('users_sekolah')->insert($operatorsekolah);
	}
}
