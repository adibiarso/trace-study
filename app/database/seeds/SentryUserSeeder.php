<?php

class SentryUserSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		Sentry::getUserProvider()->create(array(
        'email'       => 'admin@admin.com',
        'username'	  => 'admins',
        'password'    => "admins",
        'first_name'  => 'Aaron',
        'last_name'   => 'McClane',
        'activated'   => 1,
		));

		Sentry::getUserProvider()->create(array(
			'email'       => 'operator@operator.com',
			'username'	  => 'operator1',
			'password'    => "operator",
			'first_name'  => 'Amanda',
			'last_name'   => 'Kabeer',
			'activated'   => 1,
		));

		Sentry::getUserProvider()->create(array(
			'email'       => 'alumni@alumni.com',
			'username'	  => 'alumni1',
			'password'    => "alumni",
			'first_name'  => 'Jack',
			'last_name'   => 'Daniel',
			'activated'   => 1,
		));
	}
}