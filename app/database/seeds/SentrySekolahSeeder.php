<?php

class SentrySekolahSeeder extends Seeder {

	public function run()
	{
		$sekolah = array(
				'nama' => 'SMK Negeri 1 Kota Bekasi',
				'alamat' => 'Bekasi',
				'telepon' => '(022)204204',
				'akreditasi' => 'A',
				'jml_pengajar' => 20,
				'deskripsi' => 'SMK ini berada di Kota Bekasi',
				'createdby' => 1, 
				'lastmodifiedby' => 1,
				'aktif' => 1,
				'created_at' => new DateTime, 
				'updated_at' => new DateTime
			);
		DB::table('mastersekolah')->insert($sekolah);
	}
}