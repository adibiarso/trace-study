<?php

class SentryGroupSeeder extends Seeder {

	public function run()
	{
		DB::table('groups')->delete();

		Sentry::getGroupProvider()->create(array(
            'name' => 'Admin',
            'permissions' => array('admin' => 1,
                                   'sekolah.create' => 1,
                                   'sekolah.edit' => 1,
                                   'sekolah.delete' => 1,
                                   'jurusan.create' => 1,
                                   'jurusan.edit' => 1,
                                   'jurusan.delete' => 1,
                                   'operator.create' => 1,
                                   'operator.edit' => 1,
                                   'operator.delete' => 1,
                                   'alumni.create' => 1,
                                   'alumni.edit' => 1,
                                   'alumni.delete' => 1)));
        
        Sentry::getGroupProvider()->create(array(
        	'name' => 'Operator',
        	'permissions' => array('operator' => 1,
                                   'jurusan.create' => 1,
                                   'jurusan.edit' => 1,
                                   'jurusan.delete' => 1,
                                   'operator.edit' => 1,
                                   'operator.delete' => 1,
                                   'alumni.create' => 1,
                                   'alumni.edit' => 1,
                                   'alumni.delete' => 1)));
        Sentry::getGroupProvider()->create(array(
        	'name' => 'Alumni',
          	'permissions' => array('alumni' => 1,
                                    'alumni.edit' => 1,
                                   'alumni.delete' => 1)));
	}
}