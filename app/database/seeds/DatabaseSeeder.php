<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('SentryUserSeeder');
		$this->call('SentryGroupSeeder');
		$this->call('SentryUserGroupSeeder');	
		$this->call('SentrySekolahSeeder');
		$this->call('SentryJurusanSeeder');
		$this->call('SentryAlumniSeeder');	
		//$this->call('SentryJurusanSekolahSeeder');
		$this->call('SentryOperatorSekolahSeeder');
		$this->call('KegiatanAlumniSeeder');
        $this->command->info('Semua Telah Berhasil di Seed!');
	}
}
