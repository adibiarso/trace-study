<?php

class LowonganKerja extends Eloquent  {
 
	protected $table = 'lowongankerja';
	
 	public function sekolah()
	{
		return $this->belongsTo('Sekolah','sekolah_id');
	}

	public function lowongankerjacreatedby()
	{
		return $this->belongsTo('User','createdby');
	}
	public function lowongankerjamodifiedby()
	{
		return $this->belongsTo('User','lastmodifiedby');
	}
}
