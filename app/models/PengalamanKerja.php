<?php

class PengalamanKerja extends Eloquent  {
 
	protected $table = 'pengalamankerja';
	
 	public function alumni()
	{
		return $this->belongsTo('Alumni','alumni_id');
	}
}
