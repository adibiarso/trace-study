<?php

class Alumni extends Eloquent  {
 
	protected $table = 'alumni';

	public function users()
	{
		return $this->belongsTo('User','user_id');
	}
	public function userscreatedby()
	{
		return $this->belongsTo('User','createdby');
	}
	public function usersmodifiedby()
	{
		return $this->belongsTo('User','lastmodifiedby');
	}

	public function jurusan()
	{
		return $this->belongsTo('Jurusan');
	}

	public function ekskul()
	{
		return $this->hasMany('Ekskul');
	}

	public function training()
	{
		return $this->hasMany('Training');
	}

	public function pengalamankerja()
	{
		return $this->hasMany('PengalamanKerja');
	}
}