<?php

class UserSekolah extends Eloquent
{

    protected $table = 'users_sekolah';

    public function users()
    {
        return $this->belongsToMany('User', 'sekolah_id', 'operator_id');
    }

    public function sekolah_id()
    {
        return $this->hasmany('Sekolah');
    }

    public function operator_id()
    {
        return $this->hasmany('User');
    }

}
