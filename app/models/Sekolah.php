<?php

class Sekolah extends Eloquent  {
 
	protected $table = 'mastersekolah';
	
 	public function users()
	{
		return $this->belongsToMany('User','users_sekolah','sekolah_id','operator_id');
	}

	public function sekolahcreatedby()
	{
		return $this->belongsTo('User','createdby');
	}
	public function sekolahmodifiedby()
	{
		return $this->belongsTo('User','lastmodifiedby');
	}

	public function jurusan()
	{
		return $this->hasMany('Jurusan');
	}

	public function lowongankerja()
	{
		return $this->hasMany('LowonganKerja');
	}
}