<?php

class Ekskul extends Eloquent  {
 
	protected $table = 'ekskul';
	
 	public function alumni()
	{
		return $this->belongsTo('Alumni','alumni_id');
	}
}
