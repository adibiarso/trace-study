<?php

class Training extends Eloquent  {
 
	protected $table = 'training';
	
 	public function alumni()
	{
		return $this->belongsTo('Alumni','alumni_id');
	}
}
