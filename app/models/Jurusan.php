<?php

class Jurusan extends Eloquent  {
 
	protected $table = 'jurusan';
	
 	public function alumni()
	{
		return $this->hasMany('Alumni');
	}

	public function sekolah()
	{
		return $this->belongsTo('Sekolah');
	}

	public function jurusancreatedby()
	{
		return $this->belongsTo('User','createdby');
	}
	public function jurusanmodifiedby()
	{
		return $this->belongsTo('User','lastmodifiedby');
	}
}