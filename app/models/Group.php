<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

use Cartalyst\Sentry\Groups\Eloquent\Group as SentryGroupModel;
//edit confignya di -> app/config/packages/cartalyst/sentry/config.php
class Group extends SentryGroupModel {
	protected $table = 'groups';
	
    public function users()
	{
		return $this->belongsToMany('User','users_groups','group_id','user_id');
	}
}
