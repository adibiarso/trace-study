<?php namespace basicAuth\formValidation;

use Laracasts\Validation\FormValidator;

class AdminSekolahForm extends FormValidator {

  /**
   * @var array
   */
  protected $update_rules =
    [];

  /**
   * @var array
   */
  protected $rules = [

		'nama' => 'required|max:50|unique:mastersekolah',
		'alamat' => 'required|max:200',
		'telepon' => 'required|max:15',		
		'akreditasi' => 'required|max:20',
		'jml_pengajar' => 'required',
		'deskripsi' => 'required',
	];


  /**
   * @param int $id
   * @param array $input
   * @return mixed
   */
  public function validateUpdate($id, array $input)
  {
    //$this->update_rules['email'] = "required|email|unique:users,email,$id";
    //$this->rules = $this->update_rules;
    //return $this->validate($input);
  }
}


