<?php

use basicAuth\Repo\UserRepositoryInterface;
use basicAuth\formValidation\UsersEditForm;

class AlumniUsersController extends \BaseController {

    /**
     * @var $user
     */
    protected $user;

    /**
    * @var usersEditForm
    */
    protected $usersEditForm;

    /**
    * @param UsersEditForm $usersEditForm
    */
    function __construct(UserRepositoryInterface $user, UsersEditForm $usersEditForm)
    {
        $this->user = $user;
        $this->usersEditForm = $usersEditForm;

        $this->beforeFilter('currentUser', ['only' => ['show', 'edit' ,'update']]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // $user = User::findOrFail($id);
        $user = $this->user->find($id);
        $listalumni = Alumni::where('user_id','=',$id)->get();
        $listpekerjaan = Pekerjaan::where('alumni_id','=',$listalumni[0]->id)->where('aktif','=','1')->get();
        $listekskul = Ekskul::where('alumni_id','=',$listalumni[0]->id)->where('aktif','=','1')->get();
        $listtraining = Training::where('alumni_id','=',$listalumni[0]->id)->where('aktif','=','1')->get();
        return View::make('protected.alumni.show', compact('listalumni','listpekerjaan','listtraining','listekskul'))->withUser($user);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // $user = User::findOrFail($id);
        $user = $this->user->find($id);

        return View::make('protected.alumni.edit')->withUser($user);
        //return $user;
    }
public function editb($id)
    {
        // $user = User::findOrFail($id);
        //$user = $this->user->find($id);
        $userb = Alumni::where('user_id','=',Sentry::getUser()->id)->get();

        $listpekerjaan = Pekerjaan::where('alumni_id','=',$userb[0]->id)->where('aktif','=','1')->get();
        $listekskul = Ekskul::where('alumni_id','=',$userb[0]->id)->where('aktif','=','1')->get();
        $listtraining = Training::where('alumni_id','=',$userb[0]->id)->where('aktif','=','1')->get();

        return View::make('protected.alumni.editb', compact('userb','listpekerjaan','listtraining','listekskul'))->withUser($userb[0]);
        //return $userb[0];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        // $user = User::findOrFail($id);
        $user = $this->user->find($id);

        if (! Input::has("password"))
        {
            $input = Input::only('email', 'first_name', 'last_name');

            $this->usersEditForm->validateUpdate($user->id, $input);

            $user->fill($input)->save();

            return Redirect::route('profiles.edit', $user->id)->withFlashMessage('User has been updated successfully!');
        }

        else
        {
            $input = Input::only('email', 'first_name', 'last_name', 'password', 'password_confirmation');

            $this->usersEditForm->validateUpdate($user->id, $input);

            $input = array_except($input, ['password_confirmation']);

            $user->fill($input)->save();

            $user->save();

            return Redirect::route('profiles.edit', $user->id)->withFlashMessage('User (and password) has been updated successfully!');
        }
    }
    public function updateb($id)
    {

        // $user = User::findOrFail($id);
        $user = Alumni::find($id);
        //$user = $user[0];
        
        
            $input = Input::all();
            
            //$this->usersEditForm->validateUpdate($user->id, $input);
            $user->nis = $input['nis'];
            $user->tempatlahir = $input['tempatlahir'];
            $user->tanggallahir = $input['tanggallahir'];
            $user->gender = $input['gender'];
            $user->nohp = $input['nohp'];
            $user->alamat = $input['alamat'];
            $user->recruitmentsekolah = $input['recruitmentsekolah'];
            $user->minatwirausaha = $input['minatwirausaha'];

            $user->save();

            //Riwayat Pekerjaan Baru
            for($i=1;$i<=$input['jmlp'];$i++){
                if($input['deletepn'.$i]==0){
                    $kerja = new Pekerjaan;
                    $kerja->alumni_id = $id;
                    $kerja->instansi = $input['pinstansin'.$i];
                    $kerja->posisi = $input['pposisin'.$i];
                    $kerja->sesuai = $input['psesuain'.$i];
                    $kerja->tahunmasuk = $input['ptahunmasukn'.$i];
                    $kerja->tahunkeluar = $input['ptahunkeluarn'.$i];
                    $kerja->jenis = $input['pjenisn'.$i];
                    $kerja->aktif = 1;
                    $kerja->save();
                }
                else{

                }

            }
            
            //Riwayat Pekerjaan Lama
            for($i=1;$i<=$input['jmll'];$i++){

                $kerja = Pekerjaan::find($input['pid'.$i]);

                if($input['deletep'.$i]==0){
                    
                    $kerja->instansi = $input['pinstansi'.$i];
                    $kerja->posisi = $input['pposisi'.$i];
                    $kerja->sesuai = $input['psesuai'.$i];
                    $kerja->tahunmasuk = $input['ptahunmasuk'.$i];
                    $kerja->tahunkeluar = $input['ptahunkeluar'.$i];
                    $kerja->jenis = $input['pjenis'.$i];

                    $kerja->save();
                }
                else{
                    $kerja->aktif = 0;

                    $kerja->save();
                }

            }

            //Riwayat Ekskul Baru
            for($i=1;$i<=$input['jmle'];$i++){
                if($input['deleteen'.$i]==0){
                    $ekskul = new Ekskul;
                    $ekskul->alumni_id = $id;
                    $ekskul->nama = $input['enaman'.$i];
                    $ekskul->tahunmasuk = $input['etahunmasukn'.$i];
                    $ekskul->tahunkeluar = $input['etahunkeluarn'.$i];
                    $ekskul->jabatan = $input['ejabatann'.$i];
                    
                    
                    $ekskul->aktif = 1;
                    $ekskul->save();
                }
                else{

                }

            }
            
            //Riwayat Ekskul Lama
            for($i=1;$i<=$input['jmlm'];$i++){

                $ekskul = Ekskul::find($input['eid'.$i]);

                if($input['deletee'.$i]==0){
                    
                    $ekskul->nama = $input['enama'.$i];
                    $ekskul->tahunmasuk = $input['etahunmasuk'.$i];
                    $ekskul->tahunkeluar = $input['etahunkeluar'.$i];
                    $ekskul->jabatan = $input['ejabatan'.$i];
                    
                    $ekskul->save();
                }
                else{
                    $ekskul->aktif = 0;

                    $ekskul->save();
                }

            }

            //Riwayat Pelatihan Baru
            for($i=1;$i<=$input['jmlpl'];$i++){
                if($input['deletepln'.$i]==0){
                    $training = new Training;
                    $training->alumni_id = $id;
                    $training->nama = $input['plnaman'.$i];
                    $training->tahun = $input['pltahunn'.$i];
                    $training->penyelenggara = $input['plpenyelenggaran'.$i];
                    
                    $training->aktif = 1;
                    $training->save();
                }
                else{

                }

            }
            
            //Riwayat Pelatihan Lama
            for($i=1;$i<=$input['jmln'];$i++){

                $training = Training::find($input['plid'.$i]);

                if($input['deletepl'.$i]==0){
                    
                    $training->nama = $input['plnama'.$i];
                    $training->tahun = $input['pltahun'.$i];
                    $training->penyelenggara = $input['plpenyelenggara'.$i];
                    
                    
                    $training->save();
                }
                else{
                    $training->aktif = 0;

                    $training->save();
                }

            }

            

            return Redirect::route('profiles.show', $user->user_id)->withFlashMessage('Data berhasil dirubah');
            
            

            //return Input::all();
        
    }


}