<?php

class OperatorController extends \BaseController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getHome()
    {
        return View::make('protected.operator.operator_dashboard');
    }

    public function getLowongan()
    {
        $listalumni = Alumni::where('user_id', '=', Sentry::getUser()->id)->get();
        $listjurusan = Jurusan::where('id', '=', $listalumni[0]->jurusan_id)->get();
        $listlowongan = LowonganKerja::where('sekolah_id', '=', $listjurusan[0]->sekolah_id)->get();
        return View::make('protected.operator.lowongan', compact('listlowongan'));
    }


}
