<?php

class OperatorLowonganController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
        //$listsekolah = UserSekolah::where('operator_id', '=', Sentry::getUser()->id)->
        //    get();
        
            
        $listsekolah = DB::table('mastersekolah')
                        ->join('users_sekolah','mastersekolah.id','=','users_sekolah.sekolah_id')
                        ->select('users_sekolah.sekolah_id')
                        ->where('users_sekolah.operator_id','=',Sentry::getUser()->id)
                        ->get();
            
            $listlowongan = LowonganKerja::where('sekolah_id', '=', $listsekolah[0]->sekolah_id)->where('aktif','=','1')->orderBy('id','desc')->
            get();
        
        return View::make('protected.operator.list_lowongan', compact('listlowongan'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('protected.operator.create_lowongan');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'instansi' => 'required|max:100',
            'posisi' => 'required|max:50',
            'deskripsi' => 'required');

        if (Input::has('link'))
        {
            $input = array(
                'instansi' => Input::get('instansi'),
                'link' => Input::get('link'),
                'posisi' => Input::get('posisi'),
                'deskripsi' => Input::get('deskripsi'));
        }
        else
        {
            $input = array(
                'instansi' => Input::get('instansi'),
                'posisi' => Input::get('posisi'),
                'deskripsi' => Input::get('deskripsi'));
        }
        $v = Validator::make($input, $rules);

        if ($v->fails())
        {
            return Redirect::to('operator/lowongan');
        }
        else
        {
            $listsekolah = UserSekolah::where('operator_id', '=', Sentry::getUser()->id)->
                get();

            $lowongan = new LowonganKerja;
            $lowongan->instansi = Input::get('instansi');
            $lowongan->sekolah_id = $listsekolah[0]->sekolah_id;
            $lowongan->posisi = Input::get('posisi');
            $lowongan->deskripsi = Input::get('deskripsi');
            $lowongan->createdby = Sentry::getUser()->id;
            $lowongan->lastmodifiedby = Sentry::getUser()->id;
            $lowongan->created_at = new Datetime;
            $lowongan->updated_at = new Datetime;
            $lowongan->aktif = '1';
            if(Input::has('link')){
                $lowongan->link = Input::get('link');
            }
            }
        $lowongan->save();
        
        return Redirect::to('operator/lowongan');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $lowongan = LowonganKerja::find($id);
        return View::make('protected.operator.edit_lowongan', compact('lowongan'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'instansi' => 'required|max:100',
            'posisi' => 'required|max:50',
            'deskripsi' => 'required');

        if (Input::has('link'))
        {
            $input = array(
                'instansi' => Input::get('instansi'),
                'link' => Input::get('link'),
                'posisi' => Input::get('posisi'),
                'deskripsi' => Input::get('deskripsi'));
        }
        else
        {
            $input = array(
                'instansi' => Input::get('instansi'),
                'posisi' => Input::get('posisi'),
                'deskripsi' => Input::get('deskripsi'));
        }
        $v = Validator::make($input, $rules);

        if ($v->fails())
        {
            return Redirect::to('operator/lowongan');
        }
        else
        {
            $listsekolah = UserSekolah::where('operator_id', '=', Sentry::getUser()->id)->
                get();

            $lowongan = LowonganKerja::find($id);
            $lowongan->instansi = Input::get('instansi');            
            $lowongan->posisi = Input::get('posisi');
            $lowongan->deskripsi = Input::get('deskripsi');            
            $lowongan->lastmodifiedby = Sentry::getUser()->id;            
            $lowongan->updated_at = new Datetime;
            $lowongan->link = Input::get('link');
            }
        $lowongan->save();
        
        return Redirect::to('operator/lowongan');
        
        }       
        
    public function deactive($id)
    {
        $lowongan = LowonganKerja::find($id);
        $lowongan->aktif = '0';
        $lowongan->save();
        return Redirect::to('operator/lowongan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
