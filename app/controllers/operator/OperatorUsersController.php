<?php

use basicAuth\Repo\UserRepositoryInterface;
use basicAuth\formValidation\AdminUsersEditForm;

class OperatorUsersController extends \BaseController
{

    /**
     * @var $user
     */
    protected $user;

    /**
     * @var adminUsersEditForm
     */
    protected $adminUsersEditForm;

    /**
     * @param AdminUsersEditForm $AdminUsersEditForm
     */
    function __construct(UserRepositoryInterface $user, AdminUsersEditForm $adminUsersEditForm)
    {
        $this->user = $user;
        $this->adminUsersEditForm = $adminUsersEditForm;

        //$this->beforeFilter('currentUser', ['only' => ['show', 'edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$users = $this->user->getAll();
        //$operator = Sentry::findGroupByName('Operator');

        //$users = Alumni::with();
        $listsekolah = DB::table('mastersekolah')->join('users_sekolah',
            'mastersekolah.id', '=', 'users_sekolah.sekolah_id')->select('users_sekolah.sekolah_id')->
            where('users_sekolah.operator_id', '=', Sentry::getUser()->id)->get();

        $users = DB::table('users')->join('alumni', 'users.id', '=', 'alumni.user_id')->
            join('jurusan', 'alumni.jurusan_id', '=', 'jurusan.id')->join('mastersekolah',
            'jurusan.sekolah_id', '=', 'mastersekolah.id')->select('users.id',
            'alumni.id as alumni_id', 'users.email', 'users.first_name', 'users.last_name',
            'jurusan.nama', 'alumni.recruitmentsekolah', 'alumni.minatwirausaha')->where('mastersekolah.id',
            '=', $listsekolah[0]->sekolah_id)->where('alumni.approval', '=', '1')->get();


        return View::make('protected.operator.list_users')->withUsers($users);
    }

    public function pending()
    {
        //$users = $this->user->getAll();
        //$operator = Sentry::findGroupByName('Operator');

        //$users = Alumni::with();
        $listsekolah = DB::table('mastersekolah')->join('users_sekolah',
            'mastersekolah.id', '=', 'users_sekolah.sekolah_id')->select('users_sekolah.sekolah_id')->
            where('users_sekolah.operator_id', '=', Sentry::getUser()->id)->get();

        $users = DB::table('users')->join('alumni', 'users.id', '=', 'alumni.user_id')->
            join('jurusan', 'alumni.jurusan_id', '=', 'jurusan.id')->join('mastersekolah',
            'jurusan.sekolah_id', '=', 'mastersekolah.id')->select('users.id',
            'alumni.id as alumni_id', 'users.email', 'users.first_name', 'users.last_name',
            'jurusan.nama', 'alumni.recruitmentsekolah', 'alumni.minatwirausaha')->where('mastersekolah.id',
            '=', $listsekolah[0]->sekolah_id)->where('alumni.approval', '=', '2')->get();


        return View::make('protected.operator.pending_users')->withUsers($users);
    }

    public function approval($id,$user)
    {
        $user = User::find($user);
        $alumni = Alumni::find($id);
        $alumni->approval = '1';
        $user->activated = '1';
        $alumni->save();
        $user->save();
        return Redirect::to('operator/pending');
    }

    public function decline($id,$user)
    {
        $user = User::find($user);
        $alumni = Alumni::find($id);
        $alumni->approval = '0';
        $user->activated = '0';
        $alumni->save();
        $user->save();
        return Redirect::to('operator/pending');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->find($id);

        $user_group = $user->getGroups()->first()->name;

        $groups = Sentry::findAllGroups();


        return View::make('protected.operator.show_user')->withUser($user)->
            withUserGroup($user_group);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);

        $groups = Sentry::findAllGroups();

        $user_group = $user->getGroups()->first()->id;

        $array_groups = [];

        foreach ($groups as $group)
        {
            $array_groups = array_add($array_groups, $group->id, $group->name);
        }

        return View::make('protected.operator.edit_user', ['user' => $user, 'groups' =>
            $array_groups, 'user_group' => $user_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $user = $this->user->find($id);


        if (!Input::has("password"))
        {
            $input = Input::only('account_type', 'email', 'first_name', 'last_name');

            $this->adminUsersEditForm->validateUpdate($user->id, $input);

            $input = array_except($input, ['account_type']);

            $user->fill($input)->save();

            $this->user->updateGroup($id, Input::get('account_type'));


            return Redirect::route('operator.profiles.edit', $user->id)->withFlashMessage('User has been updated successfully!');
        }

        else
        {
            $input = Input::only('account_type', 'email', 'first_name', 'last_name',
                'password', 'password_confirmation');

            $this->adminUsersEditForm->validateUpdate($user->id, $input);

            $input = array_except($input, ['account_type', 'password_confirmation']);

            $user->fill($input)->save();

            $user->save();

            $this->user->updateGroup($id, Input::get('account_type'));

            return Redirect::route('operator.profiles.edit', $user->id)->withFlashMessage('User (and password) has been updated successfully!');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
