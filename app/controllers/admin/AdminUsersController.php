<?php

use basicAuth\Repo\UserRepositoryInterface;
use basicAuth\formValidation\AdminUsersEditForm;

class AdminUsersController extends \BaseController {

	/**
	 * @var $user
	 */
	protected $user;

	/**
	* @var adminUsersEditForm
	*/
	protected $adminUsersEditForm;

	/**
	* @param AdminUsersEditForm $AdminUsersEditForm
	*/
	function __construct(UserRepositoryInterface $user, AdminUsersEditForm $adminUsersEditForm)
	{
		$this->user = $user;
		$this->adminUsersEditForm = $adminUsersEditForm;

		//$this->beforeFilter('currentUser', ['only' => ['show', 'edit', 'update']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->user->getAll();
		$alumnis = Alumni::all();
		$groupadmin = Sentry::findGroupByName('Admin');
		$useradmin = Sentry::findAllUsersInGroup($groupadmin);

		$groupoperator = Sentry::findGroupByName('Operator');
		$useroperator = Sentry::findAllUsersInGroup($groupoperator);
		
		$groupalumni = Sentry::findGroupByName('Alumni');
		$useralumni = Sentry::findAllUsersInGroup($groupalumni);
		return View::make('protected.admin.list_users', compact('alumni','groupadmin','groupoperator','groupalumni'))->withUsers($users)->withuseradmin($useradmin)->withuseroperator($useroperator)->withuseralumni($useralumni);
	}


	public function create()
	{	$sekolahs = Sekolah::lists('nama','id');
		return View::make('protected.admin.create_users', compact('sekolahs'));
	}

	public function store() {
        
        	$input = array(
	            'username' => Input::get('username'), 
	            'email' => Input::get('email'), 
	            'fname' => Input::get('fname'),
	            'lname' => Input::get('lname'),
	            'password' => Input::get('password'), 
	            'password_confirmation' => Input::get('password_confirmation'),
	        	'activated' => Input::get('aktifform'),
	        	'groupregister' => Input::get('groupregister'),
	        	'sekolah_id' => Input::get('sekolah_id'));
 
        if ($input['groupregister'] == 2)
        {
	        $rules = array(
	            'username' => 'required|min:4|max:20|unique:users,username', 
	            'email' => 'required|min:4|max:32|email',
	            'fname' => 'required|max:32',
	            'lname' => 'required|max:32', 
	            'password' => 'required|min:6|confirmed', 
	            'password_confirmation' => 'required',
	        	'activated' => 'required',
	        	'groupregister' => 'required',
	        	'sekolah_id' => 'required');
	    }
	    else
	    {
	    	$rules = array(
	    		'username' => 'required|min:4|max:20|unique:users,username', 
	            'email' => 'required|min:4|max:32|email',
	            'fname' => 'required|max:32',
	            'lname' => 'required|max:32', 
	            'password' => 'required|min:6|confirmed', 
	            'password_confirmation' => 'required',
	        	'activated' => 'required|min:1|max:1',
	        	'groupregister' => 'required|min:1|max:1');
	    }
 
        //Run input validation
        $v = Validator::make($input, $rules);
 
        if ($v -> fails()) 
        {
            return Redirect::to('admin/profiles/create') -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
        } 
        else 
        {
 
            try 
            {
            	if($input['activated']==1)
            	{
            		$aktivasi = true;
            	}
            	else
            	{
            		$aktivasi = false;
            	}
                //Pre activate user
                $user = Sentry::register(array(
                    'email' => $input['email'],
                    'username' => $input['username'],
                    'password' => $input['password'],
                    'first_name' => $input['fname'],
                    'last_name' => $input['lname']),
                    $aktivasi); //kalo false, need activation code
                 
 
               //masukin ke group. 3 = alumni
                $user_group = Sentry::findGroupById($input['groupregister']);
                $user -> addGroup($user_group);
                $iduser = $user->id;

                if ($input['groupregister']== 2)
       		 	{
       		 		$operatorsekolah = new OperatorSekolah;
		        	$operatorsekolah->operator_id = $iduser;
		        	$operatorsekolah->sekolah_id = Input::get('sekolah_id');
					$operatorsekolah->save();
       		 	}

                //success!
                Session::flash('success_msg', 'Sukses Dibuat');
                return Redirect::to('admin/profiles');
 
            } 
            catch (Cartalyst\Sentry\Users\LoginRequiredException $e) 
            {
                Session::flash('error_msg', 'Username/Email Required.');
                return Redirect::to('/admin/profiles/create') -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
            } 
            
            catch (Cartalyst\Sentry\Users\UserExistsException $e) 
            {
                Session::flash('error_msg', 'Email Already Exist.');
                return Redirect::to('/admin/profiles/create') -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
            }
 
        }
	}

	public function show($id)
	{
		$user = $this->user->find($id);

		$user_group = $user->getGroups()->first()->name;

		$groups = Sentry::findAllGroups();

		$alumnis = Alumni::where('user_id', '=', $id)->get();

		return View::make('protected.admin.show_user', compact('alumnis'))->withUser($user)->withUserGroup($user_group);

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);

		$groups = Sentry::findAllGroups();

		$user_group = $user->getGroups()->first()->id;

		$array_groups = [];

		foreach ($groups as $group) {
			$array_groups = array_add($array_groups, $group->id, $group->name);
		}

		return View::make('protected.admin.edit_user', ['user' => $user, 'groups' => $array_groups, 'user_group' =>$user_group]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$user = $this->user->find($id);


		if (! Input::has("password"))
		{
			$input = Input::only('account_type' , 'email', 'first_name', 'last_name');

			$this->adminUsersEditForm->validateUpdate($user->id, $input);

			$input = array_except($input, ['account_type']);

			$user->fill($input)->save();

			$this->user->updateGroup($id, Input::get('account_type'));


			return Redirect::route('admin.profiles.edit', $user->id)->withFlashMessage('User has been updated successfully!');
		}

		else
		{
			$input = Input::only('account_type', 'email', 'first_name', 'last_name', 'password', 'password_confirmation');

			$this->adminUsersEditForm->validateUpdate($user->id, $input);

			$input = array_except($input, ['account_type', 'password_confirmation']);

			$user->fill($input)->save();

			$user->save();

			$this->user->updateGroup($id, Input::get('account_type'));

			return Redirect::route('admin.profiles.edit', $user->id)->withFlashMessage('User (and password) has been updated successfully!');

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}