<?php

class AdminAlumniController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{		
		$datajurusan = Jurusan::find($id);
		return View::make('protected.admin.create_alumni', compact('datajurusan'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($id)
	{
		$pembuat = Sentry::getUser()->id;
		$input = array(
            'username' => Input::get('username'), 
            'email' => Input::get('email'), 
            'fname' => Input::get('fname'),
            'lname' => Input::get('lname'),
            'password' => Input::get('password'), 
            'password_confirmation' => Input::get('password_confirmation'),
			'activated' => Input::get('aktifform'));
 
        // Set Validation Rules
        $rules = array(
            'username' => 'required|min:4|max:20|unique:users,username',
            'email' => 'required|min:4|max:32|email',
            'fname' => 'required|max:32',
            'lname' => 'required|max:32', 
            'password' => 'required|min:6|confirmed', 
            'password_confirmation' => 'required',
        	'activated' => 'required');
 
        //Run input validation
        $v = Validator::make($input, $rules);
 
        if ($v -> passes()) 
        {	
            	if($input['activated']==1)
            	{
            		$aktivasi = true;
            	}
            	else
            	{
            		$aktivasi = false;
            	}


                $user = Sentry::register(array(
                    'email' => $input['email'],
                    'username' => $input['username'],
                    'password' => $input['password'],
                    'first_name' => $input['fname'],
                    'last_name' => $input['lname']),
                    $aktivasi); //kalo false, need activation code
                                
               //masukin ke group. 3 = alumni
                $user_group = Sentry::findGroupByName('Alumni');
                $user -> addGroup($user_group);
                $iduser = $user->id;







			$input2 = array(
            'nis' => Input::get('nis'), 
            'tanggallahir' => Input::get('tanggallahir'), 
            'tempatlahir' => Input::get('tempatlahir'),
            'gender' => Input::get('gender'),
            'fb' => Input::get('fb'), 
            'nohp' => Input::get('nohp'), 
            'alamat' => Input::get('alamat'), 
            'telepon' => Input::get('telepon'),
            'recruitmentsekolah' => Input::get('recruitmentsekolah'), 
            'statusbekerja' => Input::get('statusbekerja'), 
            'kegiatankelompok' => Input::get('kegiatankelompok'),
            'minatwirausaha' => Input::get('minatwirausaha'));
 
       		 $rules2 = array(
        	'nis' => 'required|unique:alumni,nis',
        	'tanggallahir' => 'required|date',
        	'tempatlahir' => 'required|max:50',
        	'gender' => 'required',
        	'fb' => 'required|max:29',
        	'nohp' => 'required|max:15',
        	'alamat' => 'required',
        	'telepon' => 'required|max:20',
        	'recruitmentsekolah' => 'required',
        	'statusbekerja' => 'required',
        	'kegiatankelompok' => 'required',
        	'minatwirausaha' => 'required');
	        $vv = Validator::make($input2, $rules2);
	 
	        if ($vv->passes())
	        {
	        	$alumni = new Alumni;
	        	$alumni->user_id = $iduser;
	        	$alumni->nis = Input::get('nis');
	        	$alumni->tanggallahir = Input::get('tanggallahir');
	        	$alumni->tempatlahir = Input::get('tempatlahir');
	        	$alumni->gender = Input::get('gender');
	        	$alumni->fb = Input::get('fb');
	        	$alumni->nohp = Input::get('nohp');
	        	$alumni->alamat = Input::get('alamat');
	        	$alumni->telepon = Input::get('telepon');
	        	$alumni->jurusan_id = $id;
	        	$alumni->recruitmentsekolah = Input::get('recruitmentsekolah');
	        	$alumni->statusbekerja = Input::get('statusbekerja');
	        	$alumni->kegiatankelompok = Input::get('kegiatankelompok');
	        	$alumni->createdby = $pembuat;
	        	$alumni->lastmodifiedby = $pembuat;
	        	$alumni->minatwirausaha = Input::get('minatwirausaha');
				$alumni->save();
				Session::flash('success_msg', 'Berhasil menambahkan alumni');
			}
        }
        
        return Redirect::to('admin/alumni/'.$id);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$listalumni = Alumni::where('jurusan_id', '=', $id)->get();
		$jurusan = Jurusan::find($id);
		return View::make('protected.admin.list_alumni', compact('listalumni','jurusan'));
	}

	public function detail($id)
	{
		$listalumni = Alumni::find($id);
		return View::make('protected.admin.show_alumni', compact('listalumni'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$alumniedit = Alumni::find($id);
		$jurusans = Jurusan::lists('nama','id');
		return View::make('protected.admin.edit_alumni', compact('alumniedit','jurusans'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$pembuat = Sentry::getUser()->id;
		$a = Alumni::find($id);
		$iduser = $a->user_id;
		$redirect = Input::get('jurusan_id');
		$input = array(
            'username' => Input::get('username'), 
            'email' => Input::get('email'), 
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
			'activated' => Input::get('aktifform'));
 
        // Set Validation Rules
        $rules = array(
            'username' => 'required|min:4|max:20|unique:users,username,'.$iduser.'',
            'email' => 'required|min:4|max:32|email|unique:users,email,'.$iduser.'',
            'first_name' => 'required|max:32',
            'last_name' => 'required|max:32', 
        	'activated' => 'required');
 
        //Run input validation
        $v = Validator::make($input, $rules);
 
        if ($v -> passes()) 
        {	

                $user = User::find($iduser);
	            $user->email 	= Input::get('email');
	            $user->username = Input::get('username');
	            $user->first_name = Input::get('first_name');
	            $user->last_name = Input::get('last_name');
	            $user->activated = Input::get('aktifform');
				$user->save();
                                




			$input2 = array(
            'nis' => Input::get('nis'), 
            'tanggallahir' => Input::get('tanggallahir'), 
            'tempatlahir' => Input::get('tempatlahir'),
            'gender' => Input::get('gender'),
            'fb' => Input::get('fb'), 
            'nohp' => Input::get('nohp'), 
            'alamat' => Input::get('alamat'), 
            'telepon' => Input::get('telepon'),
            'jurusan_id' => Input::get('jurusan_id'),
            'recruitmentsekolah' => Input::get('recruitmentsekolah'), 
            'statusbekerja' => Input::get('statusbekerja'), 
            'kegiatankelompok' => Input::get('kegiatankelompok'),
            'minatwirausaha' => Input::get('minatwirausaha'));
 
       		 $rules2 = array(
        	'tanggallahir' => 'required|date',
        	'tempatlahir' => 'required|max:50',
        	'gender' => 'required',
        	'fb' => 'required|max:29',
        	'nohp' => 'required|max:15',
        	'alamat' => 'required',
        	'telepon' => 'required|max:20',
        	'jurusan_id' => 'required',
        	'recruitmentsekolah' => 'required',
        	'statusbekerja' => 'required',
        	'kegiatankelompok' => 'required',
        	'minatwirausaha' => 'required');
	        $vv = Validator::make($input2, $rules2);
	 
	        if ($vv->passes())
	        {
	        	$alumni = Alumni::find($id);
	        	$alumni->nis = Input::get('nis');
	        	$alumni->tanggallahir = Input::get('tanggallahir');
	        	$alumni->tempatlahir = Input::get('tempatlahir');
	        	$alumni->gender = Input::get('gender');
	        	$alumni->fb = Input::get('fb');
	        	$alumni->nohp = Input::get('nohp');
	        	$alumni->alamat = Input::get('alamat');
	        	$alumni->telepon = Input::get('telepon');
	        	$alumni->jurusan_id = Input::get('jurusan_id');
	        	$alumni->recruitmentsekolah = Input::get('recruitmentsekolah');
	        	$alumni->statusbekerja = Input::get('statusbekerja');
	        	$alumni->kegiatankelompok = Input::get('kegiatankelompok');
	        	$alumni->createdby = $pembuat;
	        	$alumni->lastmodifiedby = $pembuat;
	        	$alumni->minatwirausaha = Input::get('minatwirausaha');
				$alumni->save();
				Session::flash('success_msg', 'Berhasil menambahkan alumni');
			}
        }
        
        return Redirect::to('admin/alumni/'.$redirect);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
