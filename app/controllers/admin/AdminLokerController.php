<?php

class AdminLokerController extends \BaseController {


	public function index()
	{		
		$datasekolah = Sekolah::all();
		$listloker = LowonganKerja::all();
		return View::make('protected.admin.list_sekolah_loker', compact('datasekolah','listloker'));
	}


	public function create($id)
	{
		//$id = id sekolah yang mau ditambah lowongan kerja
		$datasekolah = Sekolah::find($id);
		return View::make('protected.admin.create_loker', compact('datasekolah'));
	}


	public function store($id)
	{
		//$id = id sekolah yang mau ditambah lowongan kerja
		$rules = array(
			'link' => 'required|max:200',
			'instansi' => 'required|max:200',
			'posisi' => 'required|max:200',
			'deskripsi' => 'required');

		$input = array(
				'link' => Input::get('link'), 
				'instansi' => Input::get('instansi'),
				'posisi' => Input::get('posisi'),
				'deskripsi' => Input::get('deskripsi'));
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$loker = new LowonganKerja;
        	$loker->sekolah_id= $id; 
        	$loker->link = Input::get('link');
        	$loker->instansi = Input::get('instansi');
        	$loker->posisi = Input::get('posisi');
        	$loker->deskripsi = Input::get('deskripsi');     	
        	$loker->createdby = Sentry::getUser()->id;
        	$loker->lastmodifiedby = Sentry::getUser()->id;
        	$loker->aktif = Input::get('aktifform');
			$loker->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/loker/'.$id)->withFlashMessage('Lowongan Kerja Berhasil Ditambah!');
	}


	public function createGlobal()
	{

		$sekolahs = Sekolah::lists('nama','id');
		return View::make('protected.admin.create_lokerglobal', compact('sekolahs'));
	}


	public function storeGlobal()
	{
		$rules = array(
			'sekolah_id' => 'required',
			'link' => 'required|max:200',
			'instansi' => 'required|max:200',
			'posisi' => 'required|max:200',
			'deskripsi' => 'required');

		$input = array(
				'sekolah_id' => Input::get('sekolah_id'),
				'link' => Input::get('link'), 
				'instansi' => Input::get('instansi'),
				'posisi' => Input::get('posisi'),
				'deskripsi' => Input::get('deskripsi'));
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$loker = new LowonganKerja;
        	$loker->sekolah_id= Input::get('sekolah_id'); 
        	$loker->link = Input::get('link');
        	$loker->instansi = Input::get('instansi');
        	$loker->posisi = Input::get('posisi');
        	$loker->deskripsi = Input::get('deskripsi');     	
        	$loker->createdby = Sentry::getUser()->id;
        	$loker->lastmodifiedby = Sentry::getUser()->id;
        	$loker->aktif = Input::get('aktifform');
			$loker->save();
		}
		return Redirect::to('admin/loker')->withFlashMessage('Lowongan Kerja Berhasil Ditambah!');
	}

	public function show($id)
	{
		//$id = id sekolah yang mau dilihat lowongan kerjanya
		$datasekolah = Sekolah::find($id);
		$listloker = LowonganKerja::where('sekolah_id', '=', $id)->get();
		return View::make('protected.admin.list_loker', compact('listloker','datasekolah'));
	}

	public function detail($id)//detail setiap loker (show)
	{
		$dataloker = LowonganKerja::find($id);
		return View::make('protected.admin.show_loker', compact('dataloker'));
	}

	public function edit($id)
	{		
		$sekolahs = Sekolah::lists('nama','id');
		$lokeredit = LowonganKerja::find($id);
		return View::make('protected.admin.edit_loker', compact('lokeredit', 'sekolahs'));
	}


	public function update($id)
	{
		$redirect = Input::get('sekolah_id');
		$rules = array(
			'link' => 'required|max:200',
			'instansi' => 'required|max:200',
			'posisi' => 'required|max:200',
			'deskripsi' => 'required');

		$input = array(
				'link' => Input::get('link'), 
				'instansi' => Input::get('instansi'),
				'posisi' => Input::get('posisi'),
				'deskripsi' => Input::get('deskripsi'));
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$loker = LowonganKerja::find($id);
        	$loker->sekolah_id= Input::get('sekolah_id'); 
        	$loker->link = Input::get('link');
        	$loker->instansi = Input::get('instansi');
        	$loker->posisi = Input::get('posisi');
        	$loker->deskripsi = Input::get('deskripsi');
        	$loker->lastmodifiedby = Sentry::getUser()->id;
        	$loker->aktif = Input::get('aktifform');
			$loker->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/loker/'.$redirect)->withFlashMessage('Lowongan Kerja Berhasil Diedit!');
	}



}
