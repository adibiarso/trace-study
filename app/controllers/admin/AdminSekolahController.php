<?php


class AdminSekolahController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$listsekolah = Sekolah::all();
		return View::make('protected.admin.list_sekolah', compact('listsekolah'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('protected.admin.create_sekolah');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'nama' => 'required|max:50|unique:mastersekolah',
			'alamat' => 'required|max:200',
			'telepon' => 'required|max:15',		
			'akreditasi' => 'required|max:20',
			'jml_pengajar' => 'required',
			'deskripsi' => 'required',
			'logo' => 'required|image|mimes:jpg,jpeg,bmp,png|max:5000');

	
			$input = array(
				'nama' => Input::get('nama'), 
				'logo' => Input::file('logo'), 
				'alamat' => Input::get('alamat'), 
				'telepon' => Input::get('telepon'), 
				'akreditasi' => Input::get('akreditasi'),
				'jml_pengajar' => Input::get('jml_pengajar'),
				'deskripsi' => Input::get('deskripsi'));
		
		
		
		$v = Validator::make($input, $rules);

		if ($v -> fails()) 
        {
            return Redirect::to('admin/sekolah/create') -> withErrors($v);
        } 
        else 
        {	        
        	/*
        	$file = Input::file('logo');

		    $destinationPath    = 'uploads/images/'.str_random(8); // The destination were you store the image.
		    $filename           = $file->getClientOriginalName(); // Original file name that the end user used for it.
		    $mime_type          = $file->getMimeType(); // Gets this example image/png
		    $extension          = $file->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
		    $upload_success     = $file->move($destinationPath, $filename); // Now we move the file to its new home.
			gabisa di resize
			*/
			//intervention image package
		    $image = Input::file('logo');
		    $destinationPath = 'uploads/images/';
		    $filename = date('dmYHis') . '.png';

		    Image::make($image->getRealPath())->resize('300','300')->save(public_path().'/uploads/images/'.$filename);



		   $sekolah = new Sekolah;
        	$sekolah->nama = Input::get('nama');        	
        	$sekolah->alamat = Input::get('alamat');
        	$sekolah->telepon = Input::get('telepon');
        	$sekolah->logo = $destinationPath.$filename;
        	$sekolah->akreditasi = Input::get('akreditasi');
        	$sekolah->jml_pengajar = Input::get('jml_pengajar');
        	$sekolah->deskripsi = Input::get('deskripsi');
        	$sekolah->createdby = Sentry::getUser()->id;
        	$sekolah->lastmodifiedby = Sentry::getUser()->id;
        	$sekolah->aktif = true;
			$sekolah->save();
		}

			//Notification::success('The page was saved.');
		return Redirect::to('admin/sekolah')->withFlashMessage('Sekolah Berhasil Ditambah!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$listsekolah = Sekolah::find($id);

		return View::make('protected.admin.show_sekolah', compact('listsekolah'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$sekolahedit = Sekolah::find($id);
		return View::make('protected.admin.edit_sekolah', compact('sekolahedit'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (! Input::hasFile("logo"))
		{
			$input = array(
				'nama' => Input::get('nama'),
				'alamat' => Input::get('alamat'), 
				'telepon' => Input::get('telepon'), 
				'akreditasi' => Input::get('akreditasi'),
				'jml_pengajar' => Input::get('jml_pengajar'),
				'deskripsi' => Input::get('deskripsi'),
				'aktif' => Input::get('aktif'));

			$rules = array(
			'nama' => 'required|max:50|unique:mastersekolah,nama,'.$id.'',
			'alamat' => 'required|max:200',
			'telepon' => 'required|max:15',		
			'akreditasi' => 'required|max:20',
			'jml_pengajar' => 'required',
			'deskripsi' => 'required',
			'aktif' => 'required');
		}
		else
		{
			$input = array(
				'nama' => Input::get('nama'), 
				'logo' => Input::file('logo'), 
				'alamat' => Input::get('alamat'), 
				'telepon' => Input::get('telepon'), 
				'akreditasi' => Input::get('akreditasi'),
				'jml_pengajar' => Input::get('jml_pengajar'),
				'deskripsi' => Input::get('deskripsi'),
				'aktif' => Input::get('aktif'));

			$rules = array(
			'nama' => 'required|max:50|unique:mastersekolah,nama,'.$id.'',
			'alamat' => 'required|max:200',
			'telepon' => 'required|max:15',		
			'akreditasi' => 'required|max:20',
			'jml_pengajar' => 'required',
			'deskripsi' => 'required',
			'aktif' => 'required',
			'logo' => 'required|image|mimes:jpg,jpeg,bmp,png|max:5000');
		}
		
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	   
        	$sekolah = Sekolah::find($id);
        	$sekolah->nama = Input::get('nama');        	
        	$sekolah->alamat = Input::get('alamat');
        	$sekolah->telepon = Input::get('telepon');
        	$sekolah->akreditasi = Input::get('akreditasi');
        	$sekolah->jml_pengajar = Input::get('jml_pengajar');
        	$sekolah->deskripsi = Input::get('deskripsi');
        	//$sekolah->createdby = Sentry::getUser()->id;
        	$sekolah->lastmodifiedby = Sentry::getUser()->id;
        	$sekolah->aktif = Input::get('aktif');

        	if (Input::hasFile("logo"))
        	{
        	$image = Input::file('logo');
		    $destinationPath = 'uploads/images/';
		    $filename = date('dmYHis') . '.png';

		    Image::make($image->getRealPath())->resize('300','300')->save(public_path().'/uploads/images/'.$filename);

        	$sekolah->logo = $destinationPath.$filename;
			}

			$sekolah->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/sekolah/'.$id)->withFlashMessage('Sekolah Berhasil Diedit!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
