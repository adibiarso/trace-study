<?php

class AdminJurusanController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{			
	}


	public function create($id)
	{		
		$datasekolah = Sekolah::find($id);
		return View::make('protected.admin.create_jurusan', compact('datasekolah'));
	}


	public function store($id)
	{
		$rules = array(
			'nama' => 'required|max:50',
			'aktif' => 'required');

		$input = array(
				'nama' => Input::get('nama'), 
				'aktif' => Input::get('aktifform'));
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$jurusan = new Jurusan;
        	$jurusan->nama = Input::get('nama');
        	$jurusan->sekolah_id= $id;        	
        	$jurusan->createdby = Sentry::getUser()->id;
        	$jurusan->lastmodifiedby = Sentry::getUser()->id;
        	$jurusan->aktif = Input::get('aktifform');
			$jurusan->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/jurusan/'.$id)->withFlashMessage('Sekolah Berhasil Ditambah!');
	}

	public function createOperator($id)
	{		
		$datasekolah = Sekolah::find($id);
		return View::make('protected.admin.create_jurusan_operator', compact('datasekolah'));
	}


	public function storeOperator($ids)
	{
		$input = array(
	            'username' => Input::get('username'), 
	            'email' => Input::get('email'), 
	            'fname' => Input::get('fname'),
	            'lname' => Input::get('lname'),
	            'password' => Input::get('password'), 
	            'password_confirmation' => Input::get('password_confirmation'),
	        	'activated' => Input::get('aktifform'));
 
            $rules = array(
	            'username' => 'required|min:4|max:20|unique:users,username', 
	            'email' => 'required|min:4|max:32|email',
	            'fname' => 'required|max:32',
	            'lname' => 'required|max:32', 
	            'password' => 'required|min:6|confirmed', 
	            'password_confirmation' => 'required',
	        	'activated' => 'required');
 
        //Run input validation
        $v = Validator::make($input, $rules);
 
        if ($v -> fails()) 
        {
            return Redirect::to('admin/jurusan/createoperator/'.$ids) -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
        } 
        else 
        {
 
            try 
            {
            	if($input['activated']==1)
            	{
            		$aktivasi = true;
            	}
            	else
            	{
            		$aktivasi = false;
            	}
                //Pre activate user
                $user = Sentry::register(array(
                    'email' => $input['email'],
                    'username' => $input['username'],
                    'password' => $input['password'],
                    'first_name' => $input['fname'],
                    'last_name' => $input['lname']),
                    $aktivasi); //kalo false, need activation code
                 
 
               //masukin ke group. 3 = alumni
                $user_group = Sentry::findGroupByName('Operator');
                $user -> addGroup($user_group);
                $iduser = $user->id;

       		 	$operatorsekolah = new OperatorSekolah;
		        $operatorsekolah->operator_id = $iduser;
		        $operatorsekolah->sekolah_id = $ids;
				$operatorsekolah->save();

                //success!
                Session::flash('success_msg', 'Sukses Dibuat');
                return Redirect::to('admin/jurusan/'.$ids);
 
            } 
            catch (Cartalyst\Sentry\Users\LoginRequiredException $e) 
            {
                Session::flash('error_msg', 'Username/Email Required.');
                return Redirect::to('admin/jurusan/createoperator/'.$id) -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
            } 
            
            catch (Cartalyst\Sentry\Users\UserExistsException $e) 
            {
                Session::flash('error_msg', 'Email Already Exist.');
                return Redirect::to('admin/jurusan/createoperator/'.$id) -> withErrors($v) -> withInput(Input::except(array('password', 'password_confirmation')));
            }
 
        }
	}

	public function editOperator($id)
	{		
		$operatoredit = User::find($id);
		$sekolahs = Sekolah::lists('nama','id');
		return View::make('protected.admin.edit_jurusan_operator', compact('operatoredit','sekolahs'));
	}


	public function updateOperator($id)
	{		
		$input = array(
            'username' => Input::get('username'), 
            'email' => Input::get('email'), 
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
			'activated' => Input::get('aktifform'));
 
        // Set Validation Rules
        $rules = array(
            'username' => 'required|min:4|max:20|unique:users,username,'.$id.'',
            'email' => 'required|min:4|max:32|email|unique:users,email,'.$id.'',
            'first_name' => 'required|max:32',
            'last_name' => 'required|max:32', 
        	'activated' => 'required');
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$user = User::find($id);
	        $user->email 	= Input::get('email');
	        $user->username = Input::get('username');
	        $user->first_name = Input::get('first_name');
	        $user->last_name = Input::get('last_name');
	        $user->activated = Input::get('aktifform');
			$user->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/jurusan/editoperator/'.$id)->withFlashMessage('Operator berhasil di Edit!');
	}

	public function show($id) //index
	{

		$listsekolah = Sekolah::where('id', '=', $id)->get();
		$listjurusan = Jurusan::where('sekolah_id', '=', $id)->get();
		$listalumni = Alumni::all();
		return View::make('protected.admin.list_jurusan', compact('listjurusan','listalumni','listsekolah'));
	}

	public function detail($id)//detail setiap jurusan (show)
	{
		$listjurusan = Jurusan::find($id);
		return View::make('protected.admin.show_jurusan', compact('listjurusan'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{		
		$jurusanedit = Jurusan::find($id);
		return View::make('protected.admin.edit_jurusan', compact('jurusanedit'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
			'nama' => 'required|max:50',
			'aktif' => 'required');

		$input = array(
				'nama' => Input::get('nama'), 
				'aktif' => Input::get('aktifform'));
		
		$v = Validator::make($input, $rules);

		if ($v -> passes()) 
        {	        
        	$jurusan = Jurusan::find($id);
        	$jurusan->nama = Input::get('nama');       	
        	$jurusan->lastmodifiedby = Sentry::getUser()->id;
        	$jurusan->aktif = Input::get('aktifform');
			$jurusan->save();
		}

			//Notification::success('The page was saved.');

		return Redirect::to('admin/jurusan/'.$id)->withFlashMessage('Sekolah Berhasil Ditambah!');
	}

	

}
