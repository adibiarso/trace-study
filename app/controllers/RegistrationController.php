<?php

use basicAuth\Repo\UserRepositoryInterface;
use basicAuth\formValidation\RegistrationForm;

class RegistrationController extends \BaseController {

	/**
	 * @var $user
	 */
	protected $user;

	/**
	 * @var RegistrationForm
	 */
	private $registrationForm;

	function __construct(UserRepositoryInterface $user, RegistrationForm $registrationForm)
	{
		$this->user = $user;
		$this->registrationForm = $registrationForm;
	}



	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 $listjurusan = DB::table('jurusan')->join('mastersekolah', 'mastersekolah.id', '=', 'jurusan.sekolah_id')->select('jurusan.nama AS jurusan','jurusan.id','mastersekolah.nama AS sekolah')->get();
		// return $listsekolah;


		return View::make('registration.create', compact('listjurusan'));
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputt = Input::all();
		$input = Input::only('email', 'password', 'password_confirmation', 'first_name', 'last_name','username');

		$this->registrationForm->validate($input);

		$input = Input::only('email', 'password', 'first_name', 'last_name','username');
		$input = array_add($input, 'activated', false);

		$user = $this->user->create($input);

		// Find the group using the group name
    	$usersGroup = Sentry::findGroupByName('Alumni');

    	// Assign the group to the user
    	$user->addGroup($usersGroup);

    	$alumni = new Alumni;

    	$alumni->user_id = $user->id;
    	$alumni->approval = 2;
    	$alumni->createdby = $user->id;
    	$alumni->lastmodifiedby = $user->id;
    	$alumni->jurusan_id = $inputt['jurusan'];
    	
    	$alumni->save();

		return Redirect::to('login')->withFlashMessage('Alumni telah berhasil dibuat, menunggu persetujuan operator');
	}



}