<?php

use basicAuth\formValidation\LoginForm;

class SessionsController extends \BaseController {

	protected $loginForm;

	function __construct(LoginForm $loginForm)
	{
		$this->loginForm = $loginForm;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$this->loginForm->validate($input = Input::only('email', 'password'));
		$inputs = array(
            'identity' => Input::get('identity'), 
            'password' => Input::get('password'));
        //Since user can enter username,email we cannot have email validator
        $rules = array(
            'identity' => 'required|min:4|max:32', 
            'password' => 'required|min:6');
        
        if (filter_var(Input::get('identity'), FILTER_VALIDATE_EMAIL)) 
        {
            $field = 'email';
        } 
        else
        {
            $field = 'username';
        }

        if (! User::where($field, Input::get('identity')) -> count() == 1) 
        {
            return Redirect::back()->withInput(Input::except('password'))->withErrorMessage('User does not exist');
        }

        Config::set('cartalyst/sentry::users.login_attribute',$field); 

        $v = Validator::make($inputs, $rules);
        if ($v -> fails()) 
        {
            return Redirect::to('/login') -> withErrors($v) -> withInput(Input::except('password'));
        } 
        else 
        {
            try 
            {
            	$credentials = array($field => Input::get('identity'), 'password' => Input::get('password'));
                $users = Sentry::authenticate($credentials, Input::has('remember'));
            }
           
           	catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			   	return Redirect::back()->withInput()->withErrorMessage('User not found');
			}

            catch (Cartalyst\Sentry\Users\WrongPasswordException $e) 
            {
                return Redirect::back()->withInput()->withErrorMessage('Wrong password, try again');
            } 
			
			catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
			   	return Redirect::back()->withInput()->withErrorMessage('User Not Activated.');
			}

		// Logged in successfully - redirect based on type of user
		$user = Sentry::getUser();
	    $admin = Sentry::findGroupByName('Admin');
	    $alumni = Sentry::findGroupByName('Alumni');
	    $operator = Sentry::findGroupByName('Operator');

	    if ($user->inGroup($admin)) return Redirect::to('admin');
	    elseif ($user->inGroup($operator)) return Redirect::intended('operator');
	    elseif ($user->inGroup($alumni)) return Redirect::intended('/');

		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id=null)
	{
		Sentry::logout();

		return Redirect::home();
	}

}